use Mix.Config

config :kaffy,
  admin_title: "Lèche-vitrine Admin",
  otp_app: :admin,
  ecto_repo: Legendary.Admin.Repo,
  extensions: [
    Legendary.Admin.Kaffy.EditorExtension
  ],
  router: Legendary.Admin.Router,
  resources: &Legendary.Admin.Kaffy.Config.create_resources/1

config :admin, Legendary.Admin,
  resources: [
    auth: [
      name: "Auth",
      resources: [
        user: [schema: Legendary.Auth.User, admin: Legendary.Auth.UserAdmin]
      ]
    ],
    billing: [
      name: "Billing",
      resources: [
        order: [schema: App.Billing.Order, admin: App.Billing.OrderAdmin]
      ]
    ],
    cart: [
      name: "Carts",
      resources: [
        product: [schema: App.Cart.CartLine]
      ]
    ],
    catalog: [
      name: "Catalog",
      resources: [
        product: [schema: App.Catalog.Product, admin: App.Catalog.ProductAdmin]
      ]
    ],
    content: [
      name: "Content",
      resources: [
        post: [
          schema: Legendary.Content.Post,
          admin: Legendary.Content.PostAdmin,
          label: "Posts and Pages",
          id_column: :name
        ],
        comment: [schema: Legendary.Content.Comment, admin: Legendary.Content.CommentAdmin]
      ]
    ],
    delivery: [
      name: "Delivery",
      resources: [
        run: [schema: App.Delivery.Run, admin: App.Delivery.RunAdmin],
        address: [schema: App.Delivery.Address],
        run_participant: [
          schema: App.Delivery.RunParticipant,
          admin: App.Delivery.RunParticipantAdmin
        ]
      ]
    ],
    system: [
      name: "System",
      resources: [
        # We don't want a resource here.  We just want the
        # `SystemAdmin` module to add links to the sidebar... but I
        # can't figure out how to do just that, so here's a dummy
        # resource.
        ignore_this: [admin: App.SystemAdmin]
      ]
    ]
  ]
