defmodule App.MixProject do
  use Mix.Project

  def project do
    [
      app: :app,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.7",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {App.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:admin, in_umbrella: true},
      {:bypass, "~> 2.1", only: [:test]},
      {:content, in_umbrella: true},
      {:core, in_umbrella: true},
      {:decimal, "~> 2.0"},
      {:ecto_sql, "~> 3.6"},
      {:excoveralls, "~> 0.10", only: [:dev, :test]},
      {:finch, "~> 0.8"},
      {:floki, ">= 0.30.0"},
      {:gettext, "~> 0.11"},
      {:google_api_storage, "~> 0.29.0"},
      {:goth, "~> 1.3-rc"},
      {:hackney, "~> 1.0"},
      {:jason, "~> 1.0"},
      {:nimble_csv, "~> 1.1"},
      {:number, "~> 1.0"},
      {:oban, "~> 2.1"},
      {:phoenix, "~> 1.5.9"},
      {:phoenix_ecto, "~> 4.3"},
      {:phoenix_html, "~> 3.0"},
      {:phoenix_live_dashboard, "~> 0.5.0"},
      {:phoenix_live_reload, "~> 1.3", only: :dev},
      {:phoenix_live_view, "~> 0.16.0", override: true},
      {:plug_cowboy, "~> 2.0"},
      {:postgrex, ">= 0.0.0"},
      {:telemetry_metrics, "~> 0.4"},
      {:telemetry_poller, "~> 0.4"},
      {:tesla, "~> 1.4"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get", "cmd npm install --prefix assets"],
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"],
      "npm.install": ["cmd npm install --prefix assets"]
    ]
  end
end
