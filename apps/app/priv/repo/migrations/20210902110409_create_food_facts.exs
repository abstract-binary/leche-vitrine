defmodule App.Repo.Migrations.CreateFoodFacts do
  use Ecto.Migration

  def change do
    create table(:food_facts, primary_key: false) do
      add(:barcode, :text, null: false, primary_key: true)
      add(:data, :jsonb, null: false)

      timestamps()
    end
  end
end
