defmodule App.Repo.Migrations.AddProductsSearchMv do
  use Ecto.Migration

  # https://nathanmlong.com/2018/01/fast-fulltext-search-with-ecto-and-postgresql/
  def up do
    execute("""
      CREATE MATERIALIZED VIEW products_search_mv AS
      SELECT
        id,
        wholesaler,
        product_code,
        brand,
        description,
        (
        setweight(to_tsvector(brand), 'A') ||
        setweight(to_tsvector(description), 'B')
        ) AS document
      FROM products
    """)

    # This powers the full-text search
    create(index("products_search_mv", ["document"], using: :gin))

    # This allows us to update the materialized view CONCURRENTLY.
    create(unique_index("products_search_mv", [:id]))

    # These trigramme indices power the ilike search (because tsquery
    # search don't handle substrings well).
    execute("CREATE EXTENSION pg_trgm")

    execute(
      "CREATE INDEX products_search_mv_brand_trgm_index ON products_search_mv USING gin (brand gin_trgm_ops)"
    )

    execute(
      "CREATE INDEX products_search_mv_description_trgm_index ON products_search_mv USING gin (description gin_trgm_ops)"
    )
  end

  def down do
    execute("DROP MATERIALIZED VIEW products_search_mv")
    execute("DROP EXTENSION pg_trgm")
  end
end
