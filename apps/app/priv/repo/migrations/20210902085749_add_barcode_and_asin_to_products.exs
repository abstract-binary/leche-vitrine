defmodule App.Repo.Migrations.AddBarcodeAndAsinToProducts do
  use Ecto.Migration

  def change do
    # Columns are nullable.
    alter table(:products) do
      add(:barcode, :text)
      add(:asin, :text)
    end

    # I don't think these can be unique indices because the barcodes
    # refer to individual units, but what wholesalers sell are bundles
    # of units, so it's quite likely that differently sized bundles
    # will have the same barcodes.
    create(index(:products, [:barcode]))
    create(index(:products, [:asin]))
  end
end
