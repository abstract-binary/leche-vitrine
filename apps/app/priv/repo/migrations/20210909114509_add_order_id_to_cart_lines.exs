defmodule App.Repo.Migrations.AddOrderIdToCartLines do
  use Ecto.Migration

  def change do
    alter table(:cart_lines) do
      add(:order_id, :uuid)
    end
  end
end
