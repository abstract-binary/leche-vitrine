defmodule App.Repo.Migrations.AlterCartLinesIndices do
  @moduledoc false
  use Ecto.Migration

  def up do
    drop index(:cart_lines, [:user_id, :product_id])
    create(unique_index(:cart_lines, [:user_id, :product_id, :order_id]))
  end

  def down do
    drop index(:cart_lines, [:user_id, :product_id, :order_id])
    create(unique_index(:cart_lines, [:user_id, :product_id]))
  end
end
