defmodule App.Repo.Migrations.CreateRunParticipants do
  use Ecto.Migration

  def change do
    create table(:run_participants) do
      add(:run_id, references(:runs, type: :uuid, on_delete: :nothing))
      add(:user_id, references(:users, on_delete: :nothing))

      timestamps()
    end

    create(unique_index(:run_participants, [:run_id, :user_id]))
  end
end
