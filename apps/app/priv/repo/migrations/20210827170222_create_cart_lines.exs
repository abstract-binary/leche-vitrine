defmodule App.Repo.Migrations.CreateCartLines do
  use Ecto.Migration

  def change do
    create table(:cart_lines) do
      add(:temp_user_id, :uuid)
      add(:quantity, :integer, null: false)
      add(:user_id, references(:users, on_delete: :delete_all))
      add(:product_id, references(:products, on_delete: :delete_all), null: false)

      timestamps()
    end

    create(index(:cart_lines, [:user_id]))
    create(index(:cart_lines, [:product_id]))
    create(unique_index(:cart_lines, [:user_id, :product_id]))
    create(unique_index(:cart_lines, [:temp_user_id, :product_id]))

    create(
      constraint(:cart_lines, :cart_lines_must_reference_user_or_temp_user,
        check:
          "user_id IS NOT NULL OR temp_user_id IS NOT NULL AND (user_id IS NULL OR temp_user_id IS NULL)"
      )
    )
  end
end
