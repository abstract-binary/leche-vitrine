defmodule App.Repo.Migrations.AlterCartLineOrderIdToTypeId do
  use Ecto.Migration

  def change do
    execute("DELETE FROM cart_lines")

    alter table(:cart_lines) do
      remove(:order_id)
      add(:order_id, references(:orders, on_delete: :delete_all))
    end

    create(index(:cart_lines, [:order_id]))
  end
end
