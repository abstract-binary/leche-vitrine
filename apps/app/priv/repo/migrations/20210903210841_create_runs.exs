defmodule App.Repo.Migrations.CreateRuns do
  use Ecto.Migration

  def change do
    create table(:runs, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:wholesaler, :string)
      add(:order_cutoff, :date)
      add(:estimated_delivery, :date)
      add(:minimum_order, :decimal)
      add(:minimum_order_per_participant, :decimal)
      # user_id, aka distributor
      add(:user_id, references(:users, on_delete: :delete_all), null: false)
      add(:address_id, references(:addresses, on_delete: :delete_all), null: false)

      timestamps()
    end

    create(index(:runs, [:user_id]))
    create(index(:runs, [:address_id]))
  end
end
