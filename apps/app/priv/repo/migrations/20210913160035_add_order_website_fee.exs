defmodule App.Repo.Migrations.AddOrderWebsiteFee do
  use Ecto.Migration

  def change do
    alter table(:orders) do
      add(:website_fee, :decimal, null: false)
    end
  end
end
