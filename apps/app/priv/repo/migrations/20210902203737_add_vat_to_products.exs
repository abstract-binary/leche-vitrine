defmodule App.Repo.Migrations.AddVatToProducts do
  @moduledoc false
  use Ecto.Migration

  def change do
    # Columns are nullable. Make a guess about the infinity catalog rates (0:
    # zero rate, 1: reduced rate, 2: standard rate).
    alter table(:products) do
      add(:vat_per_case, :decimal)
      add(:vat_rating, :decimal)
    end
  end
end
