defmodule App.Repo.Migrations.AddProductsMainThumbnail do
  use Ecto.Migration

  def change do
    alter table(:products) do
      add(:main_thumbnail, :text)
    end
  end
end
