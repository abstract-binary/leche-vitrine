defmodule App.Repo.Migrations.CreateAddresses do
  @moduledoc false

  use Ecto.Migration

  def change do
    create table(:addresses) do
      add :country, :string
      add :full_name, :string
      add :phone_number, :string
      add :postcode, :string
      add :address_line_1, :string
      add :address_line_2, :string
      add :city, :string
      add :delivery_instruction, :text
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:addresses, [:user_id])
  end
end
