defmodule App.Repo.Migrations.CreateProducts do
  @moduledoc false

  use Ecto.Migration

  def change do
    create table(:products) do
      add(:wholesaler, :text, null: false)
      add(:brand, :text, null: false)
      add(:description, :text, null: false)
      add(:product_code, :text, null: false)
      add(:rrp, :decimal)
      add(:case_price, :decimal)
      add(:units_per_case, :decimal)
      add(:case_size, :text)
      add(:unit, :string)

      timestamps()
    end

    create(index("products", [:wholesaler]))
    create(index("products", [:brand]))
    create(index("products", [:description]))
    create(index("products", [:wholesaler, :product_code]))
  end
end
