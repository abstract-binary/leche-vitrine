defmodule App.Repo.Migrations.ProductsAddPackSize do
  @moduledoc false

  use Ecto.Migration

  def change do
    alter table(:products) do
      add(:pack_size, :string)
    end
  end
end
