defmodule App.Repo.Migrations.CreateOrderTable do
  use Ecto.Migration

  def change do
    create table(:orders) do
      add(:run_id, references(:runs, type: :uuid, on_delete: :delete_all))
      add(:total_price, :decimal, null: false)
      add(:total_vat, :decimal, null: false)
      add(:user_id, references(:users, on_delete: :delete_all))
      add(:status, :text)
      timestamps()
    end

    create(index(:orders, [:user_id]))
    create(index(:orders, [:run_id]))
  end
end
