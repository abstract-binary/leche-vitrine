defmodule App.InfinityCatalogTest do
  @moduledoc false

  use App.DataCase

  alias App.Catalog.Product
  alias App.InfinityCatalog

  @short_csv """
  Active as a number,Product code,concatprodsize for export,organic,product description,RRP,brand,Change Marker,Case price,Vat Marker,Vat per case,Barcode inner as text,units case,pk size,unit,Vat rating
  1,1001,6x500g,organic,Arborio Rice - white,2.12,Infinity Foods,,9.55,,0,5028869010010,6,500,g,0
  1,1002,6x500g,organic,Black Rice,3.26,Infinity Foods,,14.65,,0,5028869010027,6,500,g,0
  1,891203,1x6x25cm,,Dinner Candle - unscented,6.85,Heaven Scent,,4,V,0.8,503887100622,1,6x25cm,,2
  1,900498,1p,,Mooncup A4 Poster - free of charge - please quote code:,,Mooncup,,,,0,,,1,p,0
  1,760250,6x5x20g,,Multipack Strawberry Yoyo Fruit Rolls,2.31,Bear,r,10.40,,0,5060139430364,6,5x20,g,0
  1,999991,,,PLEASE EMAIL DELIVERY NOTE,,,,0,,0,,,,,0
  """

  @shorter_csv """
  Active as a number,Product code,concatprodsize for export,organic,product description,RRP,brand,Change Marker,Case price,Vat Marker,Vat per case,Barcode inner as text,units case,pk size,unit,Vat rating
  1,1001,6x500g,organic,Arborio Rice - white,2.12,Infinity Foods,,9.55,,0,5028869010010,6,500,g,0
  1,1002,6x500g,organic,Black Rice,3.26,Infinity Foods,,14.65,,0,5028869010027,6,500,g,0
  """

  test "parse csv" do
    res = InfinityCatalog.parse_csv(@short_csv)
    assert(length(res) == 5)

    assert(
      [
        %{
          active: true,
          brand: "Infinity Foods",
          case_price: 9.55,
          case_size: "6x500g",
          pack_size: "500",
          description: "Arborio Rice - white",
          organic: true,
          product_code: "1001",
          rrp: 2.12,
          unit: "g",
          units_per_case: 6
        },
        %{
          active: true,
          brand: "Infinity Foods",
          case_price: 14.65,
          case_size: "6x500g",
          pack_size: "500",
          description: "Black Rice",
          organic: true,
          product_code: "1002",
          rrp: 3.26,
          unit: "g",
          units_per_case: 6
        },
        %{
          active: true,
          brand: "Heaven Scent",
          case_price: 4.0,
          case_size: "1x6x25cm",
          description: "Dinner Candle - unscented",
          organic: false,
          product_code: "891203",
          rrp: 6.85,
          unit: "",
          units_per_case: 1
        },
        %{
          active: true,
          brand: "Bear",
          case_price: 10.4,
          case_size: "6x5x20g",
          pack_size: "5x20",
          description: "Multipack Strawberry Yoyo Fruit Rolls",
          organic: false,
          product_code: "760250",
          rrp: 2.31,
          unit: "g",
          units_per_case: 6,
          barcode: "5060139430364",
          vat_per_case: "0",
          vat_rating: "0"
        },
        %{
          active: true,
          brand: "",
          case_price: 0.0,
          case_size: "",
          description: "PLEASE EMAIL DELIVERY NOTE",
          organic: false,
          product_code: "999991",
          rrp: nil,
          unit: "",
          units_per_case: 1
        }
      ] = res
    )
  end

  test "load CSVs and check lines inserted" do
    res = InfinityCatalog.parse_csv(@short_csv)
    assert(length(res) == 5)

    # The DELIVERY NOTE row is going to fail to insert, so silence the
    # error and expect only 4 rows in the db.
    Logger.put_module_level(InfinityCatalog, :none)
    InfinityCatalog.replace_products(res)
    assert(length(App.Catalog.list_products()) == 4)

    product =
      from(p in App.Catalog.Product, where: p.product_code == "760250")
      |> Repo.one()

    assert product.description == "Multipack Strawberry Yoyo Fruit Rolls"
    assert product.case_size == "6x5x20g"
    assert product.pack_size == "5x20"

    Logger.put_module_level(InfinityCatalog, :error)

    res = InfinityCatalog.parse_csv(@shorter_csv)
    assert(length(res) == 2)
    InfinityCatalog.replace_products(res)
    assert(length(App.Catalog.list_products()) == 2)
  end

  test "we don't delete Products from other wholesalers" do
    assert Enum.empty?(App.Catalog.list_products())

    App.Repo.insert(%Product{
      wholesaler: "NOT INFINITY FOODS",
      brand: "test",
      description: "test",
      product_code: "T0001"
    })

    App.Repo.insert(%Product{
      wholesaler: "Infinity Foods",
      brand: "test",
      description: "test",
      product_code: "T0002"
    })

    InfinityCatalog.replace_products([])
    assert(length(App.Catalog.list_products()) == 1)
  end
end
