defmodule App.DeliveryTest do
  use App.DataCase

  alias App.Delivery

  describe "addresses" do
    alias App.Delivery.Address

    @create_attrs %{
      address_line_1: "some address_line_1",
      address_line_2: "some address_line_2",
      city: "some city",
      country: "some country",
      delivery_instruction: "some delivery_instruction",
      full_name: "some full_name",
      phone_number: "some phone_number",
      postcode: "some postcode",
      user_id: nil
    }

    @user1_attrs %{
      email: "foo@example.com",
      password: "foofoofo"
    }

    @user2_attrs %{
      email: "bar@example.com",
      password: "barbarba"
    }

    def address_fixture(attrs \\ %{}) do
      {:ok, address} =
        attrs
        |> Enum.into(@create_attrs)
        |> Delivery.create_address()

      address
    end

    setup do
      alias Legendary.Auth.User

      users =
        for attrs <- [@user1_attrs, @user2_attrs] do
          {:ok, user} = %User{} |> User.changeset(attrs) |> App.Repo.insert()
          user
        end

      %{users: users}
    end

    test "list_addresses_for_user works", %{users: [user1, user2 | _]} do
      address1 = address_fixture(%{@create_attrs | user_id: user1.id})
      address2 = address_fixture(%{@create_attrs | user_id: user2.id})
      assert [address1] == Delivery.list_addresses_for_user(%{id: user1.id})
      assert [address2] == Delivery.list_addresses_for_user(%{id: user2.id})
    end

    test "get_address!/1 returns the address with given id", %{users: [user1 | _]} do
      address = address_fixture(%{@create_attrs | user_id: user1.id})
      assert Delivery.get_address!(address.id) == address
    end

    test "create_address/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} =
               Delivery.create_address(%{@create_attrs | address_line_1: nil})

      assert {:error, %Ecto.Changeset{}} =
               Delivery.create_address(%{@create_attrs | user_id: nil})
    end

    test "delete_address/1 deletes the address", %{users: [user1 | _]} do
      address = address_fixture(%{@create_attrs | user_id: user1.id})
      assert {:ok, %Address{}} = Delivery.delete_address(address)
      assert_raise Ecto.NoResultsError, fn -> Delivery.get_address!(address.id) end
    end
  end

  describe "runs" do
  end
end
