defmodule App.CheckoutTest do
  @moduledoc false

  use AppWeb.ConnCase
  require Logger

  describe "checkout" do
    setup do
      products =
        for product <- sample_products() do
          {:ok, product} = App.Repo.insert(product)
          product
        end

      users =
        for user <- sample_users() do
          {:ok, user} = App.Repo.insert(user)
          user
        end

      [runner | other_users] = users

      delivery_address = sample_address(runner.id)
      {:ok, delivery_address} = App.Repo.insert(delivery_address)

      run = sample_run(delivery_address.id, runner.id)
      {:ok, run} = App.Repo.insert(run)

      %{
        users: other_users,
        runner: runner,
        delivery_address: delivery_address,
        products: products,
        run: run
      }
    end

    def create_cart_line(user_id, product_id, quantity) do
      {:ok, cart_line} =
        App.Cart.create_cart_line(%{user_id: user_id, product_id: product_id, quantity: quantity})

      cart_line
    end

    def create_order(user_id, run_id) do
      {:ok, order} =
        App.Billing.create_order(%{
          user_id: user_id,
          run_id: run_id,
          total_vat: Decimal.new(42),
          total_price: Decimal.new(42),
          website_fee: Decimal.new(42),
          status: "Pending"
        })

      order
    end

    test "setup works", %{products: [product | _], users: [user | _]} do
      create_cart_line(user.id, product.id, 10)
    end

    test "Cart.checkout works", %{products: [product | _], users: [user | _], run: run} do
      line = create_cart_line(user.id, product.id, 10)
      assert line.order_id == nil

      order = create_order(user.id, run.id)

      assert :ok == App.Cart.checkout(user.id, order.id)
      line = App.Cart.get_cart_line!(line.id) |> App.Repo.preload([:order])
      assert line.order.run_id == run.id
    end

    test "Billing.checkout works", %{products: [product | _], users: [user | _], run: run} do
      line = create_cart_line(user.id, product.id, 10)
      assert line.order_id == nil

      assert {:ok, _} = App.Billing.checkout(user.id, run.id, Decimal.new(0))
      line = App.Cart.get_cart_line!(line.id) |> App.Repo.preload([:order])
      assert line.order.run_id == run.id
    end

    test "Billing.checkout rejects invalid order sizes", %{users: [user | _], run: run} do
      product = %App.Catalog.Product{
        brand: "Karma Cola",
        case_price: Decimal.new("27.2"),
        case_size: "300",
        description: "Razza Raspberry lemonade - bottle",
        id: 13_307,
        product_code: "661314",
        rrp: Decimal.new("1.81"),
        unit: "ml",
        units_per_case: Decimal.new("24"),
        wholesaler: "Infinity Foods"
      }

      {:ok, product} = App.Repo.insert(product)

      line = create_cart_line(user.id, product.id, 1)
      assert line.order_id == nil

      assert {:error, _} = App.Billing.checkout(user.id, run.id, Decimal.new(0))
    end

    test "create new cart after checkout works", %{
      products: [product | _],
      users: [user | _],
      run: run
    } do
      line = create_cart_line(user.id, product.id, 10)
      assert line.order_id == nil

      assert {:ok, _} = App.Billing.checkout(user.id, run.id, Decimal.new(0))
      line = App.Cart.get_cart_line!(line.id) |> App.Repo.preload([:order])
      assert line.order.run_id == run.id

      line = create_cart_line(user.id, product.id, 10)
      assert line.order_id == nil
    end
  end
end
