defmodule App.CartTest do
  use App.DataCase

  alias App.Cart

  describe "cart_lines" do
    alias App.Cart.CartLine

    @valid_attrs %{
      quantity: 42,
      user_id: nil,
      temp_user_id: "7488a646-e31f-11e4-aace-600308960662",
      product_id: nil,
      order_id: nil
    }
    @update_attrs %{
      quantity: 43,
      user_id: nil,
      temp_user_id: "7488a646-e31f-11e4-aace-600308960668",
      product_id: nil,
      order_id: nil
    }

    @invalid_attrs %{
      quantity: nil,
      user_id: nil,
      temp_user_id: nil,
      product_id: nil,
      order_id: nil
    }

    @user_attrs %{
      email: "foo@example.com",
      password: "foofoofo"
    }

    @user2_attrs %{
      email: "bar@example.com",
      password: "barbarba"
    }

    @product_attrs %{
      brand: "Karg",
      case_price: "120.5",
      case_size: "some case_size",
      description: "Spelt Seeded Crispbread",
      product_code: "0001",
      rrp: "120.5",
      unit: "some unit",
      units_per_case: "120.5",
      wholesaler: "some wholesaler"
    }

    @product2_attrs %{
      brand: "Clearspring",
      case_price: "456.7",
      case_size: "some updated case_size",
      description: "Pickled Daikon Radish",
      product_code: "0002",
      rrp: "456.7",
      unit: "some updated unit",
      units_per_case: "456.7",
      wholesaler: "some updated wholesaler"
    }

    setup do
      alias Legendary.Auth.User

      products =
        for attrs <- [@product_attrs, @product2_attrs] do
          {:ok, product} = App.Catalog.create_product(attrs)
          product
        end

      users =
        for attrs <- [@user_attrs, @user2_attrs] do
          {:ok, user} = %User{} |> User.changeset(attrs) |> App.Repo.insert()
          user
        end

      %{products: products, users: users}
    end

    def cart_line_fixture(attrs \\ %{}, %{products: [product | _]}) do
      {:ok, cart_line} =
        attrs
        |> Enum.into(%{@valid_attrs | product_id: product.id})
        |> Cart.create_cart_line()

      cart_line
    end

    test "list_cart_lines/0 returns all cart_lines", ctx do
      cart_line = cart_line_fixture(ctx)
      db_cart_lines = Cart.list_cart_lines()
      assert length(db_cart_lines) == 1
      [db_cart_line] = db_cart_lines
      assert db_cart_line.id == cart_line.id
      assert db_cart_line.product_id == cart_line.product_id
      assert db_cart_line.temp_user_id == cart_line.temp_user_id
    end

    test "get_cart_line!/1 returns the cart_line with given id", ctx do
      cart_line = cart_line_fixture(ctx)
      assert Cart.get_cart_line!(cart_line.id) == cart_line
    end

    test "create_cart_line/1 with valid data creates a cart_line", %{
      products: [product | _]
    } do
      assert {:ok, %CartLine{} = cart_line} =
               Cart.create_cart_line(%{@valid_attrs | product_id: product.id})

      assert cart_line.quantity == 42
      assert cart_line.temp_user_id == "7488a646-e31f-11e4-aace-600308960662"
      assert cart_line.product_id == product.id
    end

    test "create_cart_line/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Cart.create_cart_line(@invalid_attrs)
    end

    test "update_cart_line/2 with valid data updates the cart_line",
         %{
           products: [_, product2 | _]
         } = ctx do
      cart_line = cart_line_fixture(ctx)

      assert {:ok, %CartLine{} = cart_line} =
               Cart.update_cart_line(cart_line, %{@update_attrs | product_id: product2.id})

      assert cart_line.quantity == 43
      assert cart_line.temp_user_id == "7488a646-e31f-11e4-aace-600308960668"
      assert cart_line.product_id == product2.id
    end

    test "update_cart_line/2 with invalid data returns error changeset", ctx do
      cart_line = cart_line_fixture(ctx)
      assert {:error, %Ecto.Changeset{}} = Cart.update_cart_line(cart_line, @invalid_attrs)
      assert cart_line == Cart.get_cart_line!(cart_line.id)
    end

    test "delete_cart_line/1 deletes the cart_line", ctx do
      cart_line = cart_line_fixture(ctx)
      assert {:ok, %CartLine{}} = Cart.delete_cart_line(cart_line)
      assert_raise Ecto.NoResultsError, fn -> Cart.get_cart_line!(cart_line.id) end
    end

    test "change_cart_line/1 returns a cart_line changeset", ctx do
      cart_line = cart_line_fixture(ctx)
      assert %Ecto.Changeset{} = Cart.change_cart_line(cart_line)
    end

    test "cannot create two cart lines for the same user/product pair", %{
      products: [product | _]
    } do
      assert {:ok, %CartLine{}} = Cart.create_cart_line(%{@valid_attrs | product_id: product.id})

      assert {:error, _} = Cart.create_cart_line(%{@valid_attrs | product_id: product.id})
    end

    test "count_cart_lines_for_user works", %{
      products: [product, product2 | _],
      users: [user | _]
    } do
      assert {:ok, _} = Cart.create_cart_line(%{@valid_attrs | product_id: product.id})
      assert {:ok, _} = Cart.create_cart_line(%{@valid_attrs | product_id: product2.id})

      assert {:ok, _} =
               Cart.create_cart_line(%{
                 @valid_attrs
                 | product_id: product.id,
                   temp_user_id: @update_attrs.temp_user_id
               })

      assert {:ok, _} =
               Cart.create_cart_line(%{
                 @valid_attrs
                 | product_id: product2.id,
                   temp_user_id: nil,
                   user_id: user.id
               })

      assert 2 == Cart.count_cart_lines_for_user(@valid_attrs.temp_user_id)
      assert 1 == Cart.count_cart_lines_for_user(@update_attrs.temp_user_id)
      assert 1 == Cart.count_cart_lines_for_user(user)
    end

    test "list_cart_lines_for_user works", %{
      products: [product, product2 | _],
      users: [user, user2]
    } do
      assert {:ok, _} = Cart.create_cart_line(%{@valid_attrs | product_id: product.id})
      assert {:ok, _} = Cart.create_cart_line(%{@valid_attrs | product_id: product2.id})

      assert [] == Cart.list_cart_lines_for_user(nil)

      assert {:ok, _} =
               Cart.create_cart_line(%{
                 @valid_attrs
                 | product_id: product.id,
                   temp_user_id: nil,
                   user_id: user.id
               })

      assert {:ok, _} =
               Cart.create_cart_line(%{
                 @valid_attrs
                 | product_id: product2.id,
                   temp_user_id: nil,
                   user_id: user2.id
               })

      assert [%CartLine{}, %CartLine{}] = Cart.list_cart_lines_for_user(@valid_attrs.temp_user_id)
      assert [%CartLine{}] = Cart.list_cart_lines_for_user(user)
      assert [%CartLine{}] = Cart.list_cart_lines_for_user(user2)
    end

    test "update_cart_line_for_user_product works", %{
      products: [product | _],
      users: [user | _]
    } do
      assert {:ok, :added} = Cart.update_cart_line_for_user_product(user, product.id, 10)
      assert [%CartLine{quantity: 10}] = Cart.list_cart_lines_for_user(user)
      assert {:ok, :updated} = Cart.update_cart_line_for_user_product(user, product.id, 20)
      assert [%CartLine{quantity: 20}] = Cart.list_cart_lines_for_user(user)
      assert {:ok, :deleted} = Cart.update_cart_line_for_user_product(user, product.id, 0)
      assert [] = Cart.list_cart_lines_for_user(user)
    end

    test "update_cart_line_for_user_product rejects negative quantities", %{
      products: [product | _],
      users: [user | _]
    } do
      assert {:error, _} = Cart.update_cart_line_for_user_product(user, product.id, -10)
      assert [] = Cart.list_cart_lines_for_user(user)
    end
  end
end
