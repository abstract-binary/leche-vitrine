defmodule App.CatalogTest do
  use App.DataCase

  alias App.Catalog

  describe "products" do
    alias App.Catalog.Product

    @valid_attrs %{
      brand: "Karg",
      case_price: "120.5",
      case_size: "some case_size",
      description: "Spelt Seeded Crispbread",
      product_code: "0001",
      rrp: "120.5",
      unit: "some unit",
      units_per_case: "120.5",
      wholesaler: "some wholesaler"
    }
    @update_attrs %{
      brand: "Clearspring",
      case_price: "456.7",
      case_size: "some updated case_size",
      description: "Pickled Daikon Radish",
      product_code: "0002",
      rrp: "456.7",
      unit: "some updated unit",
      units_per_case: "456.7",
      wholesaler: "some updated wholesaler"
    }
    @invalid_attrs %{
      brand: nil,
      case_price: nil,
      case_size: nil,
      description: nil,
      product_code: nil,
      rrp: nil,
      unit: nil,
      units_per_case: nil,
      wholesaler: nil
    }

    def product_fixture(attrs \\ %{}) do
      {:ok, product} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Catalog.create_product()

      product
    end

    test "list_products/0 returns all products" do
      product = product_fixture()
      assert Catalog.list_products() == [product]
    end

    test "get_product!/1 returns the product with given id" do
      product = product_fixture()
      assert Catalog.get_product!(product.id) == product
    end

    test "create_product/1 with valid data creates a product" do
      assert {:ok, %Product{} = product} = Catalog.create_product(@valid_attrs)
      assert product.brand == "Karg"
      assert product.case_price == Decimal.new("120.5")
      assert product.case_size == "some case_size"
      assert product.description == "Spelt Seeded Crispbread"
      assert product.product_code == "0001"
      assert product.rrp == Decimal.new("120.5")
      assert product.unit == "some unit"
      assert product.units_per_case == Decimal.new("120.5")
      assert product.wholesaler == "some wholesaler"
    end

    test "create_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Catalog.create_product(@invalid_attrs)
    end

    test "update_product/2 with valid data updates the product" do
      product = product_fixture()
      assert {:ok, %Product{} = product} = Catalog.update_product(product, @update_attrs)
      assert product.brand == "Clearspring"
      assert product.case_price == Decimal.new("456.7")
      assert product.case_size == "some updated case_size"
      assert product.description == "Pickled Daikon Radish"
      assert product.product_code == "0002"
      assert product.rrp == Decimal.new("456.7")
      assert product.unit == "some updated unit"
      assert product.units_per_case == Decimal.new("456.7")
      assert product.wholesaler == "some updated wholesaler"
    end

    test "update_product/2 with invalid data returns error changeset" do
      product = product_fixture()
      assert {:error, %Ecto.Changeset{}} = Catalog.update_product(product, @invalid_attrs)
      assert product == Catalog.get_product!(product.id)
    end

    test "delete_product/1 deletes the product" do
      product = product_fixture()
      assert {:ok, %Product{}} = Catalog.delete_product(product)
      assert_raise Ecto.NoResultsError, fn -> Catalog.get_product!(product.id) end
    end

    test "change_product/1 returns a product changeset" do
      product = product_fixture()
      assert %Ecto.Changeset{} = Catalog.change_product(product)
    end

    test "query_product/1 finds reasonable results" do
      product = product_fixture()
      Catalog.refresh_products_search_mv()
      assert [product] == Catalog.query_products(Product, "crispbread")
      assert [product] == Catalog.query_products(Product, "karg")
      assert [product] == Catalog.query_products(Product, "bread")
      assert [product] == Catalog.query_products(Product, "crispbread karg")
      assert [] == Catalog.query_products(Product, "crispbreaddddd")

      # TODO-soon This isn't great.  The problem is that the substring
      # search only tries the full query string and not the individual
      # words.
      assert [] == Catalog.query_products(Product, "bread karg")
    end
  end

  describe "food_facts" do
    alias App.Catalog.FoodFact

    @valid_attrs %{barcode: "5060139430869", data: %{"_id" => "5060139430869"}}
    @update_attrs %{barcode: "5060139431194", data: %{"_id" => "5060139431194"}}
    @invalid_attrs %{barcode: nil, data: nil}

    def food_fact_fixture(attrs \\ %{}) do
      {:ok, food_fact} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Catalog.create_food_fact()

      food_fact
    end

    test "list_food_facts/0 returns all food_facts" do
      food_fact = food_fact_fixture()
      assert Catalog.list_food_facts() == [food_fact]
    end

    test "get_food_fact!/1 returns the food_fact with given barcode" do
      food_fact = food_fact_fixture()
      assert Catalog.get_food_fact!(food_fact.barcode) == food_fact
    end

    test "create_food_fact/1 with valid data creates a food_fact" do
      assert {:ok, %FoodFact{} = food_fact} = Catalog.create_food_fact(@valid_attrs)
      assert food_fact.barcode == "5060139430869"
      assert food_fact.data == %{"_id" => "5060139430869"}
    end

    test "create_food_fact/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Catalog.create_food_fact(@invalid_attrs)
    end

    test "update_food_fact/2 with valid data updates the food_fact" do
      food_fact = food_fact_fixture()
      assert {:ok, %FoodFact{} = food_fact} = Catalog.update_food_fact(food_fact, @update_attrs)
      assert food_fact.barcode == "5060139431194"
      assert food_fact.data == %{"_id" => "5060139431194"}
    end

    test "update_food_fact/2 with invalid data returns error changeset" do
      food_fact = food_fact_fixture()
      assert {:error, %Ecto.Changeset{}} = Catalog.update_food_fact(food_fact, @invalid_attrs)
      assert food_fact == Catalog.get_food_fact!(food_fact.barcode)
    end

    test "delete_food_fact/1 deletes the food_fact" do
      food_fact = food_fact_fixture()
      assert {:ok, %FoodFact{}} = Catalog.delete_food_fact(food_fact)
      assert_raise Ecto.NoResultsError, fn -> Catalog.get_food_fact!(food_fact.barcode) end
    end

    test "change_food_fact/1 returns a food_fact changeset" do
      food_fact = food_fact_fixture()
      assert %Ecto.Changeset{} = Catalog.change_food_fact(food_fact)
    end
  end
end
