defmodule AppWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common data structures and query the data layer.

  Finally, if the test case interacts with the database,
  we enable the SQL sandbox, so changes done to the database
  are reverted at the end of every test. If you are using
  PostgreSQL, you can even run database tests asynchronously
  by setting `use AppWeb.ConnCase, async: true`, although
  this option is not recommended for other databases.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      import Plug.Conn
      import Phoenix.ConnTest
      import AppWeb.ConnCase

      alias AppWeb.Router.Helpers, as: Routes

      # The default endpoint for testing
      @endpoint AppWeb.Endpoint
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(App.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(App.Repo, {:shared, self()})
    end

    App.GothHelper.start_goth_bypass()

    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end

  def sample_products do
    alias App.Catalog.Product
    alias Decimal, as: D

    [
      %App.Catalog.Product{
        brand: "Karma Cola",
        case_price: D.new("18.55"),
        case_size: "24x250ml",
        pack_size: "250",
        description: "Karma Cola - can",
        id: 5088,
        inserted_at: ~N[2021-08-23 18:37:24],
        product_code: "661290",
        rrp: D.new("1.24"),
        unit: "ml",
        units_per_case: D.new("24"),
        updated_at: ~N[2021-08-23 18:37:24],
        wholesaler: "Infinity Foods"
      },
      %App.Catalog.Product{
        brand: "Karma Cola",
        case_price: D.new("27.2"),
        case_size: "24x300ml",
        pack_size: "300",
        description: "Karma Cola - bottle",
        id: 2367,
        inserted_at: ~N[2021-08-23 18:37:18],
        product_code: "661306",
        rrp: D.new("1.81"),
        unit: "ml",
        units_per_case: D.new("24"),
        updated_at: ~N[2021-08-23 18:37:18],
        wholesaler: "Infinity Foods"
      },
      %Product{
        brand: "Karma Cola",
        case_price: D.new("18.55"),
        case_size: "24x250ml",
        pack_size: "250",
        description: "Sugar-free Karma Cola - can",
        id: 5275,
        inserted_at: ~N[2021-08-23 18:37:24],
        product_code: "661298",
        rrp: D.new("1.24"),
        unit: "ml",
        units_per_case: D.new(24),
        updated_at: ~N[2021-08-23 18:37:24],
        wholesaler: "Infinity Foods"
      },
      %App.Catalog.Product{
        brand: "Karma Cola",
        case_price: D.new("27.2"),
        case_size: "24x300ml",
        pack_size: "300",
        description: "Sugar-free Karma Cola - bottle",
        id: 4649,
        inserted_at: ~N[2021-08-23 18:37:23],
        product_code: "661317",
        rrp: D.new("1.81"),
        unit: "ml",
        units_per_case: D.new("24"),
        updated_at: ~N[2021-08-23 18:37:23],
        wholesaler: "Infinity Foods"
      },
      %App.Catalog.Product{
        brand: "Karma Cola",
        case_price: D.new("18.55"),
        case_size: "24x250ml",
        pack_size: "250",
        description: "Lemony Lemonade - can",
        id: 1445,
        inserted_at: ~N[2021-08-23 18:37:16],
        product_code: "661295",
        rrp: D.new("1.24"),
        unit: "ml",
        units_per_case: D.new("24"),
        updated_at: ~N[2021-08-23 18:37:16],
        wholesaler: "Infinity Foods"
      },
      %App.Catalog.Product{
        brand: "Karma Cola",
        case_price: D.new("27.2"),
        case_size: "24x300ml",
        pack_size: "300",
        description: "Lemony Lemonade - bottle",
        id: 5152,
        inserted_at: ~N[2021-08-23 18:37:24],
        product_code: "661316",
        rrp: D.new("1.81"),
        unit: "ml",
        units_per_case: D.new("24"),
        updated_at: ~N[2021-08-23 18:37:24],
        wholesaler: "Infinity Foods"
      },
      %App.Catalog.Product{
        brand: "Karma Cola",
        case_price: D.new("18.55"),
        case_size: "24x250ml",
        pack_size: "250",
        description: "Summer Orangeade - can",
        id: 3785,
        inserted_at: ~N[2021-08-23 18:37:21],
        product_code: "661300",
        rrp: D.new("1.24"),
        unit: "ml",
        units_per_case: D.new("24"),
        updated_at: ~N[2021-08-23 18:37:21],
        wholesaler: "Infinity Foods"
      },
      %App.Catalog.Product{
        brand: "Karma Cola",
        case_price: D.new("27.2"),
        case_size: "24x300ml",
        pack_size: "300",
        description: "Razza Raspberry lemonade - bottle",
        id: 3307,
        inserted_at: ~N[2021-08-23 18:37:20],
        product_code: "661314",
        rrp: D.new("1.81"),
        unit: "ml",
        units_per_case: D.new("24"),
        updated_at: ~N[2021-08-23 18:37:20],
        wholesaler: "Infinity Foods"
      }
    ]
  end

  def sample_address(user_id) do
    %App.Delivery.Address{
      address_line_1: "32 Brodway Market",
      address_line_2: "",
      city: "London",
      country: "England",
      delivery_instruction: "",
      full_name: "The Butcher Shop",
      phone_number: "00 44 123 456 789",
      postcode: "E8 4QJ",
      user_id: user_id
    }
  end

  def sample_users do
    alias Legendary.Auth.User

    [
      %User{email: "foo@example.com", password: "foofoofo"},
      %User{email: "bar@example.com", password: "barbarba"}
    ]
  end

  def sample_run(address_id, user_id) do
    alias Decimal, as: D

    %App.Delivery.Run{
      address_id: address_id,
      minimum_order: D.new(250),
      minimum_order_per_participant: D.new(50),
      order_cutoff: ~D[2021-09-15],
      estimated_delivery: ~D[2021-09-15],
      user_id: user_id,
      wholesaler: "Infinity Foods"
    }
  end
end
