defmodule App.GothHelper do
  @moduledoc """
  Helpers for setting up a fake GCE auth endpoint.
  """

  def start_goth_bypass do
    {:dummy_credentials, port} =
      Application.fetch_env!(:app, :google_application_credentials_json)

    bypass = Bypass.open(port: port)

    Bypass.stub(bypass, "POST", "/", fn conn ->
      body = ~s|{"access_token":"dummy","expires_in":3599,"token_type":"Bearer"}|
      Plug.Conn.resp(conn, 200, body)
    end)
  end
end
