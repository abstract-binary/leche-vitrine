defmodule AppWeb.SearchControllerTest do
  use AppWeb.ConnCase

  def fixture do
    for product <- sample_products() do
      {:ok, _} = App.Repo.insert(product)
    end

    App.Catalog.refresh_products_search_mv()
  end

  describe "index" do
    test "lists all products", %{conn: conn} do
      fixture()

      conn = get(conn, Routes.search_path(conn, :index))
      assert html_response(conn, 200) =~ "#{length(sample_products())} results"
    end

    test "'cola' matches all results", %{conn: conn} do
      fixture()

      conn = get(conn, Routes.search_path(conn, :index, query: "cola"))
      assert html_response(conn, 200) =~ "#{length(sample_products())} results"

      conn = get(conn, Routes.search_path(conn, :index), %{"search" => %{"query" => "cola"}})
      assert html_response(conn, 200) =~ "#{length(sample_products())} results"
    end

    test "'raspber' matches some", %{conn: conn} do
      fixture()

      conn = get(conn, Routes.search_path(conn, :index, query: "raspber"))
      assert html_response(conn, 200) =~ "1 result"

      conn = get(conn, Routes.search_path(conn, :index), %{"search" => %{"query" => "raspber"}})
      assert html_response(conn, 200) =~ "1 result"
    end

    test "'pepsi' matches nothing", %{conn: conn} do
      fixture()

      conn = get(conn, Routes.search_path(conn, :index, query: "pepsi"))
      assert html_response(conn, 200) =~ "0 results"

      conn = get(conn, Routes.search_path(conn, :index), %{"search" => %{"query" => "pepsi"}})
      assert html_response(conn, 200) =~ "0 results"
    end
  end
end
