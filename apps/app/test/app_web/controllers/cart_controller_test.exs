defmodule AppWeb.CartControllerTest do
  use AppWeb.ConnCase

  alias App.Cart

  describe "index" do
    setup do
      products =
        for product <- sample_products() do
          {:ok, product} = App.Repo.insert(product)
          product
        end

      App.Catalog.refresh_products_search_mv()

      users =
        for user <- sample_users() do
          {:ok, user} = App.Repo.insert(user)
          user
        end

      %{products: products, users: users}
    end

    # This needs to be called every time we need a logged-in
    # connection.  This is because Phoenix "recycles" connections, so
    # the assigns get lost after each use of the connection.
    defp login(conn, user) do
      conn
      |> recycle()
      |> Pow.Plug.assign_current_user(user, otp_app: :app)
    end

    test "list empty cart", %{conn: conn, users: [user | _]} do
      conn = get(conn |> login(user), Routes.cart_path(conn, :index))
      assert html_response(conn, 200) =~ "Your cart is empty"
    end

    test "list empty cart (temp user)", %{conn: conn} do
      conn = get(conn, Routes.cart_path(conn, :index))
      assert html_response(conn, 200) =~ "Your cart is empty"
    end

    test "update manually and list cart", %{
      conn: conn,
      products: [product, product2 | _],
      users: [user, user2 | _]
    } do
      {:ok, :added} = Cart.update_cart_line_for_user_product(user, product.id, 10)
      {:ok, :added} = Cart.update_cart_line_for_user_product(user2, product2.id, 10)

      conn = get(conn |> login(user), Routes.cart_path(conn, :index))
      assert html_response(conn, 200) =~ product.description
      # Can't see a different user's cart
      assert not (html_response(conn, 200) =~ product2.description)
    end

    test "update and list cart", %{
      conn: conn,
      products: [product, product2 | _],
      users: [user, user2 | _]
    } do
      conn =
        post(
          conn |> login(user),
          Routes.cart_path(conn, :update, %{
            "cart" => %{"product" => "#{product.id}", "qty" => "10"}
          })
        )

      conn =
        post(
          conn |> login(user2),
          Routes.cart_path(conn, :update, %{
            "cart" => %{"product" => "#{product2.id}", "qty" => "10"}
          })
        )

      conn = get(conn |> login(user), Routes.cart_path(conn, :index))
      assert html_response(conn, 200) =~ product.description
      # Can't see a different user's cart
      assert not (html_response(conn, 200) =~ product2.description)
    end

    test "update and list cart (temp user)", %{
      conn: conn,
      products: [product, product2 | _],
      users: [_, user2 | _]
    } do
      # Add an item to user2's cart (we have to do this first because,
      # otherwise, the items will be transfered from the temp cart to
      # user2's cart)
      conn =
        post(
          conn |> login(user2),
          Routes.cart_path(conn, :update, %{
            "cart" => %{"product" => "#{product2.id}", "qty" => "10"}
          })
        )

      # Add an item to the temp user's cart
      conn =
        post(
          conn,
          Routes.cart_path(conn, :update, %{
            "cart" => %{"product" => "#{product.id}", "qty" => "10"}
          })
        )

      # Check that temp user can see its own cart
      conn = get(conn, Routes.cart_path(conn, :index))
      assert html_response(conn, 200) =~ product.description
      # Temp user can't see a different user's cart
      assert not (html_response(conn, 200) =~ product2.description)
    end

    test "transfer items from temp cart to user cart", %{
      conn: conn,
      products: [product | _],
      users: [user1, user2 | _]
    } do
      # Add an item to the temp user's cart
      conn =
        post(
          conn,
          Routes.cart_path(conn, :update, %{
            "cart" => %{"product" => "#{product.id}", "qty" => "10"}
          })
        )

      # Check that temp user can see its own cart
      conn = get(conn, Routes.cart_path(conn, :index))
      assert html_response(conn, 200) =~ product.description

      # We now login as user1, so we should still see the temp cart
      conn = get(conn |> login(user1), Routes.cart_path(conn, :index))
      assert html_response(conn, 200) =~ product.description

      # The temp cart should now be empty
      conn = get(conn, Routes.cart_path(conn, :index))
      assert html_response(conn, 200) =~ "Your cart is empty"

      # The cart for a different user should also be empty
      conn = get(conn |> login(user2), Routes.cart_path(conn, :index))
      assert html_response(conn, 200) =~ "Your cart is empty"
    end
  end
end
