defmodule AppWeb.AddressControllerTest do
  use AppWeb.ConnCase

  alias App.Delivery

  @create_attrs %{
    address_line_1: "some address_line_1",
    address_line_2: "some address_line_2",
    city: "some city",
    country: "some country",
    delivery_instruction: "some delivery_instruction",
    full_name: "some full_name",
    phone_number: "some phone_number",
    postcode: "some postcode",
    user_id: nil
  }

  def address_fixture(attrs \\ %{}) do
    {:ok, address} =
      attrs
      |> Enum.into(@create_attrs)
      |> Delivery.create_address()

    address
  end

  setup do
    users =
      for user <- sample_users() do
        {:ok, user} = App.Repo.insert(user)
        user
      end

    %{users: users}
  end

  # This needs to be called every time we need a logged-in
  # connection.  This is because Phoenix "recycles" connections, so
  # the assigns get lost after each use of the connection.
  defp login(conn, user) do
    conn
    |> recycle()
    |> Pow.Plug.assign_current_user(user, otp_app: :app)
  end

  describe "index" do
    test "lists all addresses for user", %{conn: conn, users: [user | _]} do
      conn = get(conn |> login(user), Routes.address_path(conn, :index))
      assert html_response(conn, 200) =~ "Addresses"
    end

    test "other users addresses do not show up", %{conn: conn, users: [user1, user2 | _]} do
      _address1 = address_fixture(%{@create_attrs | user_id: user1.id, full_name: "User 1"})
      _address2 = address_fixture(%{@create_attrs | user_id: user2.id, full_name: "User 2"})

      conn = get(conn |> login(user1), Routes.address_path(conn, :index))
      assert html_response(conn, 200) =~ "User 1"
      assert not (html_response(conn, 200) =~ "User 2")
    end
  end

  describe "new address" do
    test "renders form", %{conn: conn, users: [user | _]} do
      conn = get(conn |> login(user), Routes.address_path(conn, :new))
      assert html_response(conn, 200) =~ "Save"
    end

    test "creates new address", %{conn: conn, users: [user | _]} do
      conn =
        post(conn |> login(user), Routes.address_path(conn, :create), %{
          "address" => %{
            country: "GB",
            full_name: "Joe Random",
            phone_number: "0000000",
            postcode: "EC1 0AA",
            address_line_1: "Random Tower",
            city: "London"
          }
        })

      assert redirected_to(conn) =~ "/addresses/"
      assert [_] = Delivery.list_addresses_for_user(user)
    end
  end

  describe "login is required for operations" do
    test "new", %{conn: conn} do
      conn = get(conn, Routes.address_path(conn, :new))
      assert html_response(conn, 302) =~ "redirected"
    end

    test "show", %{conn: conn, users: [user | _]} do
      address = address_fixture(%{@create_attrs | user_id: user.id})
      conn = get(conn, Routes.address_path(conn, :show, address.id))
      assert html_response(conn, 302) =~ "redirected"
    end

    test "edit", %{conn: conn, users: [user | _]} do
      address = address_fixture(%{@create_attrs | user_id: user.id})
      conn = get(conn, Routes.address_path(conn, :edit, address.id))
      assert html_response(conn, 302) =~ "redirected"
    end

    test "delete", %{conn: conn, users: [user | _]} do
      address = address_fixture(%{@create_attrs | user_id: user.id})
      conn = get(conn, Routes.address_path(conn, :delete, address.id))
      assert html_response(conn, 302) =~ "redirected"
    end

    test "update", %{conn: conn, users: [user | _]} do
      address = address_fixture(%{@create_attrs | user_id: user.id})
      conn = get(conn, Routes.address_path(conn, :update, address.id))
      assert html_response(conn, 302) =~ "redirected"
    end
  end

  describe "logged in users cannot access other users' addresses" do
    test "show", %{conn: conn, users: [user1, user2 | _]} do
      address2 = address_fixture(%{@create_attrs | user_id: user2.id})
      conn = get(conn |> login(user1), Routes.address_path(conn, :show, address2.id))
      assert conn.status == 403
    end

    test "edit", %{conn: conn, users: [user1, user2 | _]} do
      address2 = address_fixture(%{@create_attrs | user_id: user2.id})
      conn = get(conn |> login(user1), Routes.address_path(conn, :edit, address2.id))
      assert conn.status == 403
    end

    test "delete", %{conn: conn, users: [user1, user2 | _]} do
      address2 = address_fixture(%{@create_attrs | user_id: user2.id})
      conn = get(conn |> login(user1), Routes.address_path(conn, :delete, address2.id))
      assert conn.status == 403
    end

    test "update", %{conn: conn, users: [user1, user2 | _]} do
      address2 = address_fixture(%{@create_attrs | user_id: user2.id})
      conn = get(conn |> login(user1), Routes.address_path(conn, :update, address2.id))
      assert conn.status == 403
    end
  end
end
