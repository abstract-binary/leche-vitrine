defmodule AppWeb.RunControllerTest do
  use AppWeb.ConnCase

  alias App.Delivery

  @run_create_attrs %{
    estimated_delivery: ~D[2010-04-17],
    minimum_order: "120.5",
    minimum_order_per_participant: "120.5",
    order_cutoff: ~D[2010-04-17],
    wholesaler: "some wholesaler",
    user_id: nil,
    address_id: nil
  }

  def run_fixture(attrs \\ %{}) do
    {:ok, run} =
      attrs
      |> Enum.into(@run_create_attrs)
      |> Delivery.create_run()

    run
  end

  @address_create_attrs %{
    address_line_1: "some address_line_1",
    address_line_2: "some address_line_2",
    city: "some city",
    country: "some country",
    delivery_instruction: "some delivery_instruction",
    full_name: "some full_name",
    phone_number: "some phone_number",
    postcode: "some postcode",
    user_id: nil
  }

  def address_fixture(attrs \\ %{}) do
    {:ok, address} =
      attrs
      |> Enum.into(@address_create_attrs)
      |> Delivery.create_address()

    address
  end

  # This needs to be called every time we need a logged-in
  # connection.  This is because Phoenix "recycles" connections, so
  # the assigns get lost after each use of the connection.
  defp login(conn, user) do
    conn
    |> recycle()
    |> Pow.Plug.assign_current_user(user, otp_app: :app)
  end

  describe "index" do
    setup do
      users =
        for user <- sample_users() do
          {:ok, user} = App.Repo.insert(%{user | roles: ["organizer"]})
          address = address_fixture(%{@address_create_attrs | user_id: user.id})
          %{user: user, address: address}
        end

      %{users: users}
    end

    test "lists all runs for user", %{conn: conn, users: [%{user: user} | _]} do
      conn = get(conn |> login(user), Routes.run_path(conn, :index))
      assert html_response(conn, 200) =~ "My runs"
    end

    test "other users runs do not show up", %{conn: conn, users: [user1, user2 | _]} do
      _run1 =
        run_fixture(%{
          @run_create_attrs
          | user_id: user1.user.id,
            address_id: user1.address.id,
            wholesaler: "WHOLESALER 1"
        })

      _run2 =
        run_fixture(%{
          @run_create_attrs
          | user_id: user2.user.id,
            address_id: user2.address.id,
            wholesaler: "WHOLESALER2 2"
        })

      conn = get(conn |> login(user1.user), Routes.run_path(conn, :index))
      assert html_response(conn, 200) =~ "WHOLESALER 1"
      assert not (html_response(conn, 200) =~ "WHOLESALER 2")
    end
  end

  describe "join" do
    setup do
      users =
        for user <- sample_users() do
          {:ok, user} = App.Repo.insert(user)

          user
        end

      [runner | other_users] = users

      delivery_address = address_fixture(%{@address_create_attrs | user_id: runner.id})

      run =
        run_fixture(%{
          @run_create_attrs
          | user_id: runner.id,
            address_id: delivery_address.id
        })

      %{users: other_users, runner: runner, delivery_address: delivery_address, run: run}
    end

    test "setup works", %{users: [user | _], run: run} do
      assert user.id != run.user_id
    end

    test "login/join redirects", %{conn: conn, users: [user | _], run: run} do
      conn = get(conn |> login(user), Routes.join_path(conn, :join, run.id))
      assert redirected_to(conn, 302) =~ "/"
    end

    test "join redirects", %{conn: conn, run: run} do
      conn = get(conn, Routes.join_path(conn, :join, run.id))
      assert redirected_to(conn, 302) =~ "/"
    end

    test "join works", %{conn: conn, run: run, users: [user | _]} do
      assert !App.Delivery.joined_run(%{user_id: user.id, run_id: run.id})
      _conn = get(conn |> login(user), Routes.join_path(conn, :join, run.id))
      assert App.Delivery.joined_run(%{user_id: user.id, run_id: run.id})
    end

    test "join idempotent", %{conn: conn, run: run, users: [user | _]} do
      assert !App.Delivery.joined_run(%{user_id: user.id, run_id: run.id})

      conn = get(conn |> login(user), Routes.join_path(conn, :join, run.id))
      assert redirected_to(conn, 302) =~ "/"
      assert App.Delivery.joined_run(%{user_id: user.id, run_id: run.id})

      conn = get(conn |> login(user), Routes.join_path(conn, :join, run.id))
      assert redirected_to(conn, 302) =~ "/"
      assert App.Delivery.joined_run(%{user_id: user.id, run_id: run.id})
    end
  end
end
