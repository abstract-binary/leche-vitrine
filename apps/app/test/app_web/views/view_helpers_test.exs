defmodule AppWeb.ViewHelpersTest do
  use AppWeb.ConnCase

  import AppWeb.ViewHelpers
  import Phoenix.HTML, only: [safe_to_string: 1]

  alias Decimal, as: D

  test "format_price doesn't change price" do
    assert safe_to_string(format_price(nil)) =~ "£—"
    assert safe_to_string(format_price(D.new("1.23"))) =~ "1"
    assert safe_to_string(format_price(D.new("1.23"))) =~ "23"
    assert safe_to_string(format_price(D.new("2.4"))) =~ "2"
    assert safe_to_string(format_price(D.new("2.4"))) =~ "40"
  end

  test "can_be_converted_to_decimal" do
    assert valid_decimal("100") == true
    assert valid_decimal(Decimal.new("100")) == true
    assert valid_decimal(100) == true

    assert valid_decimal(100.0) == false
    assert valid_decimal("one cubic meter") == false
  end
end
