defmodule AppWeb.Router do
  use AppWeb, :router
  use Pow.Phoenix.Router

  use Pow.Extension.Phoenix.Router,
    extensions: [PowResetPassword, PowEmailConfirmation]

  import Phoenix.LiveDashboard.Router

  alias Legendary.AuthWeb.Plugs.RequireAdmin

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_live_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(:put_layout, {AppWeb.LayoutView, :app})
    plug(:put_root_layout, {AppWeb.LayoutView, :root})
    plug(AppWeb.Plugs.TempUserId)
    plug(AppWeb.Plugs.FetchCart)
    plug(AppWeb.Plugs.FetchParticipatingRuns)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  pipeline :require_admin do
    plug(RequireAdmin)
  end

  pipeline :require_organizer do
    plug(AppWeb.Plugs.RequireOrganizer)
  end

  pipeline :require_auth do
    plug(Pow.Plug.RequireAuthenticated, error_handler: Pow.Phoenix.PlugErrorHandler)
  end

  # Other scopes may use custom stacks.
  # scope "/api", AppWeb do
  #   pipe_through :api
  # end

  if Mix.env() in [:dev, :test] do
    forward("/sent_emails", Bamboo.SentEmailViewerPlug)
  end

  scope "/" do
    pipe_through([:browser, :require_auth, :require_admin])

    live_dashboard("/live-dashboard", metrics: AppWeb.Telemetry)
  end

  if Mix.env() == :e2e do
    forward("/end-to-end", Legendary.CoreWeb.Plug.TestEndToEnd, otp_app: :app)
  end

  scope "/" do
    pipe_through(:browser)

    pow_session_routes()

    get "/signup", AppWeb.RegistrationController, :new
    post "/signup", AppWeb.RegistrationController, :create

    # Don't ask why this is called :show.
    get "/confirm-email/:token", AppWeb.EmailConfirmationController, :show,
      as: :pow_email_confirmation_confirmation

    get "/reset-password/new", AppWeb.ResetPasswordController, :new,
      as: :pow_reset_password_reset_password

    post "/reset-password", AppWeb.ResetPasswordController, :reset

    get "/reset-password/:token", AppWeb.ResetPasswordController, :edit
    put "/reset-password/:token", AppWeb.ResetPasswordController, :save_new_password
  end

  scope "/" do
    pipe_through(:browser)

    get("/search", AppWeb.SearchController, :index)
    get("/search/:query", AppWeb.SearchController, :index)
    get("/cart", AppWeb.CartController, :index)
    post("/cart", AppWeb.CartController, :update)
  end

  scope "/" do
    pipe_through(:browser)
    pipe_through(:require_auth)

    get("/account", AppWeb.AccountController, :index)
    get("/account/edit", AppWeb.AccountController, :edit)
    put("/account", AppWeb.AccountController, :update)

    resources("/addresses", AppWeb.AddressController)

    get("/order", AppWeb.OrderController, :index)
    get("/order/:id", AppWeb.OrderController, :show)

    resources("/runs", AppWeb.RunController)

    get("/join/:uuid", AppWeb.JoinController, :join)
    delete("/join/:uuid", AppWeb.JoinController, :leave)
    get("/my-runs", AppWeb.JoinController, :index)

    post("/billing/checkout", AppWeb.BillingController, :checkout)
  end

  scope "/" do
    pipe_through(:browser)
    pipe_through(:require_auth)
    pipe_through(:require_organizer)
  end

  use Legendary.Core.Routes
  use Legendary.Admin.Routes
  use Legendary.Content.Routes
end
