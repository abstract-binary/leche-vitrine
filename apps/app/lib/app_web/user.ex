defmodule AppWeb.User do
  @moduledoc """
  Helpers for dealing with users.
  """

  @doc "Returns the current `Pow` user or the `temp_user_id` string."
  @spec current_user_or_temp_user_id(Plug.Conn.t()) :: %Legendary.Auth.User{} | String.t()
  def current_user_or_temp_user_id(conn) do
    case Pow.Plug.current_user(conn) do
      %{id: _} = user -> user
      _ -> AppWeb.Plugs.TempUserId.get(conn)
    end
  end
end
