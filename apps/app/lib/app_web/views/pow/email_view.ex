defmodule AppWeb.Pow.EmailView do
  use Phoenix.View,
    root: "lib/app_web/templates",
    namespace: AppWeb,
    pattern: "**/*"

  import Phoenix.HTML, only: [raw: 1], warn: false
end
