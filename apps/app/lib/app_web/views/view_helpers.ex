defmodule AppWeb.ViewHelpers do
  @moduledoc """
  Random assortment functions to help with the construction of
  multiple views.  Basically, if you're writing complicated code in a
  template, move it to its own view.  If the code is shared by
  multiple views, then move it here.
  """

  import Phoenix.HTML

  require Decimal

  @doc """
  Format a price nicely with a pound sign in front, two decimals, and
  the whole part larger than the rest.

  Note that the sizing of the text is done *relative* here.  So, you
  should specify `text-base`, `text-sm`, `text-lg` or something in the
  parent container.
  """
  @spec format_price(nil | Decimal.t(), Keyword.t()) :: Phoenix.HTML.safe()
  def format_price(px, opts \\ [])

  def format_price(nil, opts) do
    classes = Keyword.get(opts, :classes, [])

    ~E"""
    <div class="<%= Enum.join(classes, " ")%>">£—</div>
    """
  end

  def format_price(px, opts) when Decimal.is_decimal(px) do
    classes = Keyword.get(opts, :classes, [])

    whole = Decimal.round(px, 0, :down)
    two_decimals = Decimal.sub(px, whole) |> Decimal.mult(100) |> Decimal.round(0, :down)

    whole = whole |> Decimal.to_string()

    two_decimals = two_decimals |> Decimal.to_string()

    two_decimals =
      cond do
        String.length(two_decimals) == 0 -> "00"
        String.length(two_decimals) == 1 -> "#{two_decimals}0"
        true -> two_decimals
      end

    ~E"""
    <div class="flex flex-row align-top <%= Enum.join(classes, " ")%>">
    <span style="font-size: 0.75em">£</span><span class="font-bold" style="font-size: 1.25em"><%= whole %></span><span style="font-size: 0.75em"><%= two_decimals %></span>
    </div>
    """
  end

  @spec format_price_plain(Decimal.t()) :: String.t()
  def format_price_plain(px) when Decimal.is_decimal(px) do
    whole = Decimal.round(px, 0, :down)
    two_decimals = Decimal.sub(px, whole) |> Decimal.mult(100) |> Decimal.round(0, :down)

    whole = whole |> Decimal.to_string()

    two_decimals = two_decimals |> Decimal.to_string()

    two_decimals =
      cond do
        String.length(two_decimals) == 0 -> "00"
        String.length(two_decimals) == 1 -> "#{two_decimals}0"
        true -> two_decimals
      end

    "£#{whole}.#{two_decimals}"
  end

  @doc """
  Format a quantity nicely with thousands separators.
  """
  @spec format_quantity(nil | Decimal.t()) :: Phoenix.HTML.safe()
  def format_quantity(qty)

  def format_quantity(nil) do
    ~E"-"
  end

  def format_quantity(qty) when Decimal.is_decimal(qty) do
    if Decimal.integer?(qty) do
      Number.Delimit.number_to_delimited(qty, precision: 0)
    else
      Number.Delimit.number_to_delimited(qty)
    end
  end

  @doc """
  Tests if an argument can be converted to decimals.  This is useful
  for `Product.pack_size` because it's a string which is usually a
  number, but not always.
  """
  @spec valid_decimal(any()) :: bool
  def valid_decimal(x) when is_integer(x) or is_binary(x) or Decimal.is_decimal(x) do
    _ = Decimal.new(x)
    true
  rescue
    _ -> false
  end

  def valid_decimal(_), do: false
end
