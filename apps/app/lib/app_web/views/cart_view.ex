defmodule AppWeb.CartView do
  use AppWeb, :view

  @doc """
  Returns the total savings for the given `CartLine`s.  Returns nil if
  the savings are not positive.
  """
  def total_savings(cart_lines) do
    zero = Decimal.new(0)

    total_savings =
      Enum.reduce(cart_lines, zero, fn cart_line, total ->
        Decimal.add(total, CartLine.savings(cart_line) || zero)
      end)

    if Decimal.compare(total_savings, 0) == :gt,
      do: total_savings,
      else: nil
  end

  @doc """
  Returns the total VAT for the given `CartLine`s.
  """
  def total_vat(cart_lines) do
    zero = Decimal.new(0)

    Enum.reduce(cart_lines, zero, fn cart_line, total ->
      Decimal.add(total, CartLine.total_vat(cart_line) || zero)
    end)
  end
end
