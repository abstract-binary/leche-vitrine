defmodule AppWeb.Plugs.FetchParticipatingRuns do
  @moduledoc """
  Fetches the runs that the current user is participating in and
  stores them in the assigns.
  """

  @behaviour Plug

  import Plug.Conn

  @impl Plug
  def init(opts) do
    opts
  end

  @impl Plug
  def call(conn, _opts) do
    fetch_runs(conn)
  end

  defp fetch_runs(conn) do
    case Pow.Plug.current_user(conn) do
      nil ->
        assign(conn, :participating_runs, [])

      %{id: _} = user ->
        assign(
          conn,
          :participating_runs,
          App.Delivery.list_participating_runs_for_user(user)
        )
    end
  end
end
