defmodule AppWeb.Plugs.RequireOrganizer do
  @moduledoc """
  A plug that returns 403 unauthorized if the user is not an
  organizer.
  """
  import Plug.Conn
  alias Legendary.Auth.{Roles, User}

  def init(opts) do
    opts
  end

  def call(conn, _opts) do
    with user = %User{} <- Pow.Plug.current_user(conn),
         true <- Roles.has_role?(user, "organizer") do
      conn
    else
      _ ->
        conn
        |> send_resp(403, "Unauthorized")
        |> halt()
    end
  end
end
