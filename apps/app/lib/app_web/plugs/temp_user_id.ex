defmodule AppWeb.Plugs.TempUserId do
  @moduledoc """
  Every connection should have either a `Pow.Plug.current_user` or a
  `Plug.Conn.get_session(_, :temp_user_id)`.  This plug must run after
  the `:fetch_session` plug.
  """

  @behaviour Plug

  @impl Plug
  def init(opts) do
    opts
  end

  @impl Plug
  def call(conn, _opts) do
    case Pow.Plug.current_user(conn) do
      %{} ->
        conn

      nil ->
        case get(conn) do
          nil -> Plug.Conn.put_session(conn, :temp_user_id, Ecto.UUID.generate())
          _ -> conn
        end
    end
  end

  @doc "Returns the `temp_user_id` for this connection."
  @spec get(Plug.Conn.t()) :: nil | String.t()
  def get(conn) do
    case Plug.Conn.get_session(conn, :temp_user_id) do
      str when is_binary(str) -> str
      _ -> nil
    end
  end
end
