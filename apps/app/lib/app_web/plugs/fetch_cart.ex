defmodule AppWeb.Plugs.FetchCart do
  @moduledoc """
  Fetches the cart for the current user or temp user and stores it in
  the assigns.  Must run after `AppWeb.Plugs.TempUserId`.

  If there's a cart for both the temp user and the logged-in user,
  then the temp user's cart is transfered into the logged-in user's
  cart.
  """

  @behaviour Plug

  import Plug.Conn

  alias App.Cart

  require Logger

  @impl Plug
  def init(opts) do
    opts
  end

  @impl Plug
  def call(conn, _opts) do
    conn
    |> transfer_cart_lines_from_temp_user()
    |> fetch_cart_lines()
  end

  defp fetch_cart_lines(conn) do
    user = AppWeb.User.current_user_or_temp_user_id(conn)
    cart_lines = Cart.list_cart_lines_for_user(user)

    conn
    |> assign(:cart_lines, cart_lines)
  end

  defp transfer_cart_lines_from_temp_user(conn) do
    auth_user = Pow.Plug.current_user(conn)
    temp_user_id = AppWeb.Plugs.TempUserId.get(conn)

    if not is_nil(auth_user) and not is_nil(temp_user_id) do
      case Cart.transfer_cart_lines(temp_user_id, auth_user) do
        :ok -> :ok
        {:error, error} -> Logger.error("#{error}")
      end
    end

    conn
  end
end
