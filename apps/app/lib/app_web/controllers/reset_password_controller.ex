defmodule AppWeb.ResetPasswordController do
  @moduledoc """
  The controller that gets called when a user goes to the Reset
  Password page.  This has to be a separate module from
  `RegistrationController` because that's what `pow` expects.
  """

  use AppWeb, :controller

  require Logger

  def new(conn, _params) do
    changeset = Legendary.Auth.User.reset_password_changeset(%Legendary.Auth.User{}, %{})
    render(conn, "new.html", changeset: changeset)
  end

  def reset(conn, %{"user" => user_params}) do
    case PowResetPassword.Plug.create_reset_token(conn, user_params) do
      {:ok, %{token: token, user: user}, conn} ->
        email =
          PowResetPassword.Phoenix.Mailer.reset_password(
            conn,
            user,
            Routes.reset_password_url(conn, :edit, token)
          )

        Pow.Phoenix.Mailer.deliver(conn, email)

        conn
        |> put_flash(:info, PowResetPassword.Phoenix.Messages.email_has_been_sent(conn))
        |> redirect(to: "/")

      {:error, error, conn} ->
        Logger.error("Error resetting password for #{inspect(user_params)}: #{inspect(error)}")

        conn
        |> put_flash(:error, PowResetPassword.Phoenix.Messages.user_not_found(conn))
        |> redirect(to: "/")
    end
  end

  def edit(conn, %{"token" => token}) do
    case PowResetPassword.Plug.load_user_by_token(conn, token) do
      {:ok, conn} ->
        changeset =
          Legendary.Auth.User.reset_password_changeset(conn.assigns.reset_password_user, %{})

        render(conn, "edit.html", changeset: changeset, token: token)

      {:error, conn} ->
        Logger.error("Error resetting email for token #{token}}")

        conn
        |> put_flash(:error, PowResetPassword.Phoenix.Messages.invalid_token(conn))
        |> redirect(to: "/")
    end
  end

  def save_new_password(conn, %{"token" => token, "user" => user_params}) do
    case PowResetPassword.Plug.load_user_by_token(conn, token) do
      {:ok, conn} ->
        case PowResetPassword.Plug.update_user_password(conn, user_params) do
          {:ok, _, conn} ->
            conn
            |> put_flash(:info, "Password changed successfully")
            |> redirect(to: "/")

          {:error, error, conn} ->
            Logger.error("Error resetting email for token #{token}: #{inspect(error)}")

            conn
            |> put_flash(:info, "Error changing password")
            |> redirect(to: "/")
        end

      {:error, conn} ->
        Logger.error("Error resetting email for token #{token}: #{inspect(conn)}")

        conn
        |> put_flash(:error, PowResetPassword.Phoenix.Messages.invalid_token(conn))
        |> redirect(to: "/")
    end
  end
end
