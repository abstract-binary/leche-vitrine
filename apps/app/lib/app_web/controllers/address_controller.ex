defmodule AppWeb.AddressController do
  @moduledoc """
  Manage user delivery addresses
  """
  use AppWeb, :controller

  alias App.Delivery
  alias App.Delivery.Address

  plug(:authorize when action in [:show, :edit, :update, :delete])

  def index(conn, _params) do
    user = %{id: _} = Pow.Plug.current_user(conn)
    addresses = Delivery.list_addresses_for_user(user)
    render(conn, "index.html", addresses: addresses)
  end

  def new(conn, _params) do
    changeset = Delivery.change_address(%Address{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"address" => address_params}) do
    %{id: user_id} = Pow.Plug.current_user(conn)

    case Delivery.create_address(Map.merge(address_params, %{"user_id" => user_id})) do
      {:ok, address} ->
        conn
        |> put_flash(:info, "Address created successfully.")
        |> redirect(to: Routes.address_path(conn, :show, address))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    address = Delivery.get_address!(id)
    render(conn, "show.html", address: address)
  end

  def edit(conn, %{"id" => id}) do
    address = Delivery.get_address!(id)
    changeset = Delivery.change_address(address)
    render(conn, "edit.html", address: address, changeset: changeset)
  end

  def update(conn, %{"id" => id, "address" => address_params}) do
    address = Delivery.get_address!(id)

    case Delivery.update_address(address, address_params) do
      {:ok, address} ->
        conn
        |> put_flash(:info, "Address updated successfully.")
        |> redirect(to: Routes.address_path(conn, :show, address))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", address: address, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    address = Delivery.get_address!(id)
    {:ok, _address} = Delivery.delete_address(address)

    conn
    |> put_flash(:info, "Address deleted successfully.")
    |> redirect(to: Routes.address_path(conn, :index))
  end

  defp authorize(%{params: %{"id" => id}} = conn, _options) do
    %{id: current_user_id} = Pow.Plug.current_user(conn)
    address = Delivery.get_address!(id)

    if address.user_id != current_user_id do
      conn
      |> put_status(403)
      |> text("Forbidden")
      |> halt()
    else
      conn
    end
  end
end
