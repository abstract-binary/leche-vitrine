defmodule AppWeb.AccountController do
  @moduledoc """
  Shows user account and allows them to edit it.
  """

  use AppWeb, :controller

  alias App.Delivery

  require Logger

  def index(conn, _params) do
    user = Pow.Plug.current_user(conn)
    addresses = Delivery.list_addresses_for_user(user)
    orders = App.Billing.list_orders_for_user(user)
    runs = Delivery.list_runs_for_user(user)

    render(conn, "index.html",
      user: user,
      addresses: addresses,
      orders: orders,
      runs: runs
    )
  end

  def edit(conn, _params) do
    changeset = Pow.Plug.change_user(conn)
    render(conn, "edit.html", changeset: changeset)
  end

  def update(conn, %{"user" => user_params}) do
    conn
    |> Pow.Plug.update_user(user_params)
    |> case do
      {:ok, _user, conn} ->
        conn
        |> put_flash(:info, "Account updated!")
        |> redirect(to: Routes.account_path(conn, :index))

      {:error, changeset, conn} ->
        render(conn, "edit.html", changeset: changeset)
    end
  end
end
