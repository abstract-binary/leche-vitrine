defmodule AppWeb.OrderController do
  use AppWeb, :controller

  alias App.Billing

  plug(:authorize when action in [:show])

  def index(conn, _params) do
    user = %{id: _} = Pow.Plug.current_user(conn)
    orders = App.Billing.list_orders_for_user(user)
    render(conn, "index.html", orders: orders)
  end

  def show(conn, %{"id" => id}) do
    order = Billing.get_order!(id)
    order_cart_lines = Billing.list_order_cart_lines(order)
    render(conn, "show.html", order: order, order_cart_lines: order_cart_lines)
  end

  defp authorize(%{params: %{"id" => id}} = conn, _options) do
    %{id: current_user_id} = Pow.Plug.current_user(conn)
    order = Billing.get_order!(id)

    if order.user_id != current_user_id do
      conn
      |> put_status(403)
      |> text("Forbidden")
      |> halt()
    else
      conn
    end
  end
end
