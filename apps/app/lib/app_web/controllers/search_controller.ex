defmodule AppWeb.SearchController do
  @moduledoc """
  Implements the main product search through the big search bar.

  Must run after `AppWeb.Plugs.FetchCart`.
  """

  use AppWeb, :controller

  alias App.Catalog

  def index(conn, params) do
    query =
      case params do
        %{"search" => %{"query" => query}} ->
          # URLs like "/search?search[query]=fentimans"
          query

        %{"query" => query} ->
          # URLs like "/search/coca+cola"
          query

        _ ->
          ""
      end

    cart_lines =
      for cart_line <- conn.assigns.cart_lines, into: %{} do
        {cart_line.product_id, cart_line}
      end

    products =
      for product <- Catalog.query_products(query) do
        Map.put(product, :cart_line, Map.get(cart_lines, product.id))
      end

    render(conn, "index.html", query: query, products: products)
  end
end
