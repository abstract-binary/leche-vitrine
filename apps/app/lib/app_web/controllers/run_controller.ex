defmodule AppWeb.RunController do
  use AppWeb, :controller

  alias App.Delivery
  alias App.Delivery.Run

  plug(:authorize when action in [:show, :edit, :update, :delete])

  # from(w in Wholesaler, select: {w.name, w.id})
  @wholesalers [{"Infinity Foods", "Infinity Foods"}]

  def index(conn, _params) do
    user = %{id: _} = Pow.Plug.current_user(conn)
    runs = Delivery.list_runs_for_user(user)
    render(conn, "index.html", runs: runs)
  end

  def new(conn, _params) do
    changeset = Delivery.change_run(%Run{})
    user = %{id: _} = Pow.Plug.current_user(conn)
    addresses = Delivery.list_addresses_for_user(user)

    render(conn, "new.html", changeset: changeset, wholesalers: @wholesalers, addresses: addresses)
  end

  def create(conn, %{"run" => run_params}) do
    user = %{id: _} = Pow.Plug.current_user(conn)
    addresses = Delivery.list_addresses_for_user(user)

    case Delivery.create_run(Map.merge(run_params, %{"user_id" => user.id})) do
      {:ok, run} ->
        conn
        |> put_flash(:info, "Delivery run created successfully.")
        |> redirect(to: Routes.run_path(conn, :show, run))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html",
          changeset: changeset,
          wholesalers: @wholesalers,
          addresses: addresses
        )
    end
  end

  def show(conn, %{"id" => id}) do
    run = Delivery.get_run(id)
    participants = Delivery.list_run_participants(id)
    render(conn, "show.html", run: run, participants: participants)
  end

  def edit(conn, %{"id" => id}) do
    user = %{id: _} = Pow.Plug.current_user(conn)
    addresses = Delivery.list_addresses_for_user(user)

    run = Delivery.get_run(id)
    changeset = Delivery.change_run(run)

    render(conn, "edit.html",
      run: run,
      changeset: changeset,
      wholesalers: @wholesalers,
      addresses: addresses
    )
  end

  def update(conn, %{"id" => id, "run" => run_params}) do
    run = Delivery.get_run(id)

    case Delivery.update_run(run, run_params) do
      {:ok, run} ->
        conn
        |> put_flash(:info, "Run updated successfully.")
        |> redirect(to: Routes.run_path(conn, :show, run))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", run: run, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    run = Delivery.get_run(id)
    {:ok, _run} = Delivery.delete_run(run)

    conn
    |> put_flash(:info, "Run deleted successfully.")
    |> redirect(to: Routes.run_path(conn, :index))
  end

  defp authorize(%{params: %{"id" => id}} = conn, _options) do
    %{id: current_user_id} = Pow.Plug.current_user(conn)
    run = Delivery.get_run(id)

    if run.user_id != current_user_id do
      conn
      |> put_status(403)
      |> text("Forbidden")
      |> halt()
    else
      conn
    end
  end
end
