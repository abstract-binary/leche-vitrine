defmodule AppWeb.BillingController do
  use AppWeb, :controller

  require Logger

  def checkout(
        conn,
        %{"checkout" => %{"run_id" => run_id}, "website_fee" => website_fee} = params
      ) do
    %{id: user_id} = Pow.Plug.current_user(conn)
    website_fee = Decimal.new(website_fee)
    return_to = Map.get(params, "return_to", "/")

    case App.Billing.checkout(user_id, run_id, website_fee) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Checked out!")
        |> redirect(to: Routes.order_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(
          :error,
          "Something went wrong, please contact us! #{run_id}, #{user_id} #{inspect(changeset)}"
        )
        |> redirect(to: return_to)

      {:error, msg} ->
        conn
        |> put_flash(:error, msg)
        |> redirect(to: return_to)
    end
  end
end
