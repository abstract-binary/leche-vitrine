defmodule AppWeb.CartController do
  @moduledoc """
  Show the user a cart and lets them edit it.

  This relies on the assigns set by `AppWeb.Plugs.FetchCart`.
  """

  use AppWeb, :controller
  alias App.Cart
  alias App.Cart.CartLine

  require Logger

  def index(conn, params) do
    cart_total_price_no_fee =
      Enum.reduce(conn.assigns.cart_lines, Decimal.new(0), fn cart_line, total ->
        Decimal.add(total, CartLine.total_price(cart_line))
      end)

    ten_percent_fee = Decimal.mult(Decimal.from_float(0.10), cart_total_price_no_fee)

    website_fee_options =
      [
        Decimal.new(0),
        Decimal.new(5),
        Decimal.mult(Decimal.from_float(0.05), cart_total_price_no_fee),
        ten_percent_fee
      ]
      |> Enum.sort_by(& &1, Decimal)

    website_fee =
      case params do
        %{"website_fee" => %{"website_fee" => website_fee}} -> Decimal.new(website_fee)
        _ -> ten_percent_fee
      end

    cart_total_price = Decimal.add(cart_total_price_no_fee, website_fee)

    render(conn, "index.html",
      cart_total_price: cart_total_price,
      website_fee_options: website_fee_options,
      website_fee: website_fee
    )
  end

  def update(conn, %{"cart" => %{"product" => product_id, "qty" => qty}} = params) do
    user = AppWeb.User.current_user_or_temp_user_id(conn)
    return_to = Map.get(params, "return_to", "/")

    with {product_id, ""} <- Integer.parse(product_id),
         {qty, ""} <- Integer.parse(qty),
         {:ok, res} <- Cart.update_cart_line_for_user_product(user, product_id, qty) do
      msg =
        case res do
          :added -> "Added to cart"
          :deleted -> "Removed from cart"
          _ -> "Cart updated"
        end

      redirect(conn |> put_flash(:info, msg), to: return_to)
    else
      error ->
        Logger.error("Can't parse cart update params: #{inspect(params)} (#{inspect(error)})")
        redirect(conn |> put_flash(:error, "Error updating cart"), to: return_to)
    end
  end

  def update(conn, params) do
    Logger.error("Cart update called with wrong params: #{inspect(params)}")
    redirect(conn |> put_flash(:error, "Error updating cart"), to: "/")
  end
end
