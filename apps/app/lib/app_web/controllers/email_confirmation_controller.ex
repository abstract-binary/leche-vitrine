defmodule AppWeb.EmailConfirmationController do
  @moduledoc """
  The controller that gets called when emails are confirmed.  This has
  to be a separate module from `RegistrationController` because that's
  what `pow` expects.
  """

  use AppWeb, :controller

  require Logger

  def show(conn, %{"token" => token}) do
    case PowEmailConfirmation.Plug.load_user_by_token(conn, token) do
      {:ok, conn} ->
        case PowEmailConfirmation.Plug.confirm_email(conn, %{}) do
          {:ok, _user, conn} ->
            conn
            |> put_flash(:info, "Email confirmed")
            |> redirect(to: "/")

          error ->
            Logger.error("Error confirming email for token #{token}: #{inspect(error)}")

            conn
            |> put_flash(:error, "Error confirming email")
            |> redirect(to: "/")
        end

      error ->
        Logger.error("Failed to confirm email for token #{token}: #{inspect(error)}")

        conn
        |> put_flash(:error, "Invalid reset token")
        |> redirect(to: "/")
    end
  end
end
