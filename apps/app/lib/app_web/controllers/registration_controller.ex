defmodule AppWeb.RegistrationController do
  @moduledoc """
  Controller that registers new users.
  """

  use AppWeb, :controller

  def new(conn, _params) do
    changeset = Pow.Plug.change_user(conn)

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    conn
    |> Pow.Plug.create_user(user_params)
    |> case do
      {:ok, user, conn} ->
        {:ok, user} = App.User.revalidate_email(user)
        PowEmailConfirmation.Phoenix.ControllerCallbacks.send_confirmation_email(user, conn)

        conn
        |> redirect(to: "/")

      {:error, changeset, conn} ->
        render(conn, "new.html", changeset: changeset)
    end
  end
end
