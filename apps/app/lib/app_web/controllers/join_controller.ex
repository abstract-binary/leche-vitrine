defmodule AppWeb.JoinController do
  use AppWeb, :controller

  require Logger

  def index(conn, _params) do
    user = Pow.Plug.current_user(conn)
    participating_runs = App.Delivery.list_participating_runs_for_user(user)
    render(conn, "index.html", participating_runs: participating_runs)
  end

  def leave(conn, %{"uuid" => _run_id}) do
    conn
    |> put_flash(:error, "You cannot leave this run")
    |> redirect(to: Routes.join_path(conn, :index))
  end

  def join(conn, %{"uuid" => run_id}) do
    %{id: user_id} = Pow.Plug.current_user(conn)

    case App.Delivery.join_run(%{run_id: run_id, user_id: user_id}) do
      {:ok, run} ->
        conn
        |> put_flash(:info, "Joined #{run.user.display_name}'s run!")
        |> redirect(to: "/")

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(
          :error,
          "Failed to join: #{inspect(run_id)}, #{inspect(user_id)} #{inspect(changeset)}"
        )
        |> redirect(to: "/")
    end
  end
end
