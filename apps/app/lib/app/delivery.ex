defmodule App.Delivery do
  @moduledoc """
  The Delivery context.
  """

  import Ecto.Query, warn: false
  alias App.Repo

  alias App.Delivery.Address
  alias App.Delivery.Run
  alias App.Delivery.RunParticipant

  @doc """
  Returns the list of addresses for the given user.
  """
  def list_addresses_for_user(%{id: user_id}) do
    from(a in Address, where: a.user_id == ^user_id)
    |> Repo.all()
  end

  @doc """
  Gets a single address.

  Raises `Ecto.NoResultsError` if the Address does not exist.
  """
  def get_address!(id), do: Repo.get!(Address, id)

  @doc """
  Creates a address for a given user.
  """
  def create_address(attrs \\ %{}) do
    %Address{}
    |> Address.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a address.
  """
  def update_address(%Address{} = address, attrs) do
    address
    |> Address.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a address.
  """
  def delete_address(%Address{} = address) do
    Repo.delete(address)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking address changes.
  """
  def change_address(%Address{} = address, attrs \\ %{}) do
    Address.changeset(address, attrs)
  end

  @doc """
  Returns the list of runs for the given user.
  """
  def list_runs_for_user(%{id: user_id}) do
    from(a in Run, where: a.user_id == ^user_id)
    |> Repo.all()
    |> Repo.preload([:address])
  end

  @doc """
  Returns the list of runs that a user is participating in.
  """
  def list_participating_runs_for_user(%{id: user_id}) do
    from(a in RunParticipant,
      join: r in Run,
      on: a.run_id == r.id,
      where: a.user_id == ^user_id,
      select: r
    )
    |> Repo.all()
    |> Repo.preload([:address, :user])
  end

  @doc """
  Gets a single run.

  Raises `Ecto.NoResultsError` if the Run does not exist.
  """
  def get_run(id) do
    Repo.get(Run, id)
    |> Repo.preload([:address, :user])
  end

  @doc """
  Creates a run.
  """
  def create_run(attrs \\ %{}) do
    %Run{}
    |> Run.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a run.
  """
  def update_run(%Run{} = run, attrs) do
    run
    |> Run.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a run.
  """
  def delete_run(%Run{} = run) do
    Repo.delete(run)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking run changes.
  """
  def change_run(%Run{} = run, attrs \\ %{}) do
    Run.changeset(run, attrs)
  end

  def joined_run(%{user_id: user_id, run_id: run_id}) do
    n =
      from(a in RunParticipant,
        where: a.user_id == ^user_id and a.run_id == ^run_id,
        select: count()
      )
      |> Repo.one()

    n > 0
  end

  def joined_run(_) do
    false
  end

  @doc """
  Add a participant to a delivery run
  """
  def join_run(attrs \\ %{}) do
    if joined_run(attrs) do
      {:ok, get_run(attrs.run_id)}
    else
      %RunParticipant{}
      |> RunParticipant.changeset(attrs)
      |> Repo.insert()
      |> case do
        {:ok, run} ->
          {:ok, run |> Repo.preload([:user])}

        error ->
          error
      end
    end
  end

  def list_run_participants(run_id) do
    from(a in RunParticipant, where: a.run_id == ^run_id)
    |> Repo.all()
    |> Repo.preload([:user])
  end
end
