defmodule App.Billing.Order do
  @moduledoc """
  An order, as represented in the database. Use the cart / cart-lines
  to represent the concrete list of items ordered.

  Note that the `total_price` is inclusive of VAT and `website_fee`.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "orders" do
    belongs_to(:run, App.Delivery.Run, type: :binary_id)
    belongs_to(:user, Legendary.Auth.User)
    field(:total_price, :decimal)
    field(:total_vat, :decimal)
    field(:website_fee, :decimal)
    field(:status, :string)
    timestamps()
  end

  @doc false
  def changeset(order, attrs) do
    order
    |> cast(attrs, [:run_id, :user_id, :total_price, :total_vat, :website_fee, :status])
    |> validate_required([:run_id, :user_id, :total_price, :total_vat, :website_fee])
  end
end
