defmodule App.Billing.OrderAdmin do
  @moduledoc """
  Kaffy config for `Order`.
  """

  alias App.Delivery.Run
  alias App.Repo

  def index(_) do
    [
      id: nil,
      run: %{
        value: fn order ->
          order = order |> Repo.preload(run: [:address, :user])
          Run.short_desc(order.run)
        end
      },
      user: %{
        value: fn order ->
          order = order |> Repo.preload([:user])
          order.user.email
        end
      },
      status: nil,
      total_price: nil,
      total_vat: nil,
      website_fee: nil,
      inserted_at: nil,
      updated_at: nil
    ]
  end
end
