defmodule App.Cart.CartLine do
  @moduledoc """
  A `CartLine` represents a single line in a user's cart.  The idea is
  that we don't have concrete "carts" anywhere.  Instead, we compute
  the carts dynamically by selecting all the cart lines for a
  particular user.  A side-effect is that we can only have one cart
  for each user at a time, but that seems reasonable.
  """

  use Ecto.Schema
  import Ecto.Changeset

  alias App.Catalog.Product

  schema "cart_lines" do
    field(:quantity, :integer)
    field(:temp_user_id, Ecto.UUID)
    belongs_to(:user, Legendary.Auth.User)
    belongs_to(:product, App.Catalog.Product)
    belongs_to(:order, App.Billing.Order)
    timestamps()
  end

  @doc false
  def changeset(cart_line, attrs) do
    cart_line
    |> cast(attrs, [:temp_user_id, :quantity, :user_id, :product_id, :order_id])
    |> validate_required([:quantity, :product_id])
    |> unique_constraint([:user_id, :product_id, :order_id])
    |> unique_constraint([:temp_user_id, :product_id])
    |> foreign_key_constraint(:user_id)
    |> foreign_key_constraint(:product_id)
    |> check_constraint(:temp_user_id, name: :cart_lines_must_reference_user_or_temp_user)
    |> check_constraint(:user_id, name: :cart_lines_must_reference_user_or_temp_user)
  end

  @doc "Returns the total price of this cart line"
  def total_price(cart_line) do
    case_price_with_vat =
      Decimal.add(
        cart_line.product.case_price,
        Product.vat_per_case(cart_line.product)
      )

    Decimal.mult(case_price_with_vat, cart_line.quantity)
  end

  @doc """
  Returns the savings vs RRP for this cart line.  Returns `nil` if the
  RRP is unknown or if the savings are negative.
  """
  def savings(cart_line) do
    if rrp = Product.indicative_rrp_per_case(cart_line.product) do
      savings =
        Decimal.sub(
          Decimal.mult(rrp, cart_line.quantity),
          total_price(cart_line)
        )

      if Decimal.compare(savings, 0) == :gt,
        do: savings,
        else: nil
    else
      nil
    end
  end

  @doc """
  Returns the VAT paid on this cart line.
  """
  def total_vat(cart_line) do
    Decimal.mult(Product.vat_per_case(cart_line.product), cart_line.quantity)
  end
end
