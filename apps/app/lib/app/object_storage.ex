defmodule App.ObjectStorage do
  @moduledoc """
  Interface for accessing GCE Storage, including writing objects to
  the public bucket, and getting public URLs for objects.
  """

  @spec public_bucket() :: String.t()
  def public_bucket do
    Application.fetch_env!(:app, :public_bucket)
  end

  @spec public_url(String.t()) :: String.t()
  def public_url(name) when is_binary(name) do
    "https://storage.googleapis.com/#{public_bucket()}/#{name}"
  end

  @spec list_objects() :: {:ok, [%GoogleApi.Storage.V1.Model.Object{}]} | {:error, String.t()}
  def list_objects do
    with {:ok, token} <- Goth.fetch(MyGoth),
         conn <- GoogleApi.Storage.V1.Connection.new(token.token),
         {:ok, %{items: items}} <-
           GoogleApi.Storage.V1.Api.Objects.storage_objects_list(conn, public_bucket()) do
      {:ok, items}
    else
      error -> {:error, "Failed to list objects: #{inspect(error)}"}
    end
  end

  @doc """
  Create or replace an object in GCE Storage.  You can get the public
  URL with `public_url`.  The content_type argument is important
  because that's how browsers will interpret the file.

  Note that if permissions are setup incorrectly, this function is
  going to return weird errors (e.g. it may say you're uploading to
  the wrong URL).  So far, I've found that `Storage Object Admin`
  limited to
  `resource.name.startsWith("projects/_/buckets/BUCKET_NAME")` seems
  to work; I haven't been able to get `Storage Viewer/Creator/...` to
  work.
  """
  @spec put_object(String.t(), :text | String.t(), iodata()) :: :ok | {:error, String.t()}
  def put_object(name, content_type, data) do
    metadata =
      case content_type do
        :text -> %{contentType: "text/plain; charset=UTF-8"}
        str when is_binary(str) -> %{contentType: str}
      end

    with {:ok, token} <- Goth.fetch(MyGoth),
         conn <- GoogleApi.Storage.V1.Connection.new(token.token),
         {:ok, %GoogleApi.Storage.V1.Model.Object{}} <-
           GoogleApi.Storage.V1.Api.Objects.storage_objects_insert_iodata(
             conn,
             public_bucket(),
             "multipart",
             Map.merge(%{name: name}, metadata),
             data
           ) do
      :ok
    else
      error -> {:error, "Failed to list objects: #{inspect(error)}"}
    end
  end
end
