defmodule App.Catalog.Product do
  @moduledoc """
  A `Product`, as it appears in the database.  Products from different
  wholesalers are all placed in the same table.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "products" do
    field(:brand, :string)
    field(:case_price, :decimal)
    field(:case_size, :string)
    field(:pack_size, :string)
    field(:description, :string)
    field(:product_code, :string)
    field(:rrp, :decimal)
    field(:unit, :string)
    field(:units_per_case, :decimal)
    field(:wholesaler, :string)
    field(:barcode, :string)
    field(:asin, :string)
    field(:main_thumbnail, :string)
    field(:vat_per_case, :decimal)
    field(:vat_rating, :decimal)
    timestamps()
  end

  @doc false
  def changeset(product, attrs) do
    product
    |> cast(attrs, [
      :wholesaler,
      :brand,
      :description,
      :product_code,
      :rrp,
      :case_price,
      :units_per_case,
      :case_size,
      :pack_size,
      :unit,
      :vat_per_case,
      :vat_rating
    ])
    |> validate_required([:wholesaler, :brand, :description, :product_code])
    |> unique_constraint([:wholesaler, :product_code])
  end

  def indicative_rrp_per_case(%{rrp: nil}), do: nil

  def indicative_rrp_per_case(%{rrp: rrp, units_per_case: units_per_case}) do
    Decimal.mult(rrp, units_per_case || 1)
  end

  def rrp_per_unit(%{rrp: rrp}) do
    rrp
  end

  def vat_per_case(%{vat_per_case: nil}), do: Decimal.new(0)

  @spec vat_per_case(%{:vat_per_case => any, optional(any) => any}) :: any
  def vat_per_case(%{vat_per_case: vat_per_case}) do
    vat_per_case
  end
end
