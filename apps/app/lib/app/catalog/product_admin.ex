defmodule App.Catalog.ProductAdmin do
  @moduledoc """
  Kaffy options for the `Product` tables.
  """

  def list_actions(_conn) do
    [
      reload_infinity_catalog: %{
        name: "Reload Infinity Foods Catalog",
        action: fn _conn, _products -> reload_infinity_catalog() end
      },
      refresh_products_search_mv: %{
        name: "Refresh products_search_mv",
        action: fn _conn, _products -> refresh_products_search_mv() end
      },
      fetch_missing_food_facts: %{
        name: "Fetch missing food facts",
        action: fn _conn, _products -> App.Catalog.fetch_missing_food_facts() end
      },
      fetch_missing_images: %{
        name: "Fetch missing images",
        action: fn _conn, _products -> App.Catalog.fetch_missing_images() end
      }
    ]
  end

  defp reload_infinity_catalog do
    with :ok <- App.InfinityCatalog.download_catalog(),
         :ok <- App.InfinityCatalog.load_csv() do
      :ok
    else
      {:error, error} ->
        {:error, "Error reloading Infinity Foods Catalog: #{inspect(error)}"}
    end
  end

  defp refresh_products_search_mv do
    case App.Catalog.refresh_products_search_mv() do
      {:ok, _} -> :ok
      {:error, error} -> {:error, "Error refreshing products_search_mv: #{inspect(error)}"}
    end
  end
end
