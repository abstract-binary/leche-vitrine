defmodule App.Catalog.FoodFact do
  @moduledoc """
  An entry from `openfoodfacts.org`.  For instance:
  https://world.openfoodfacts.org/product/5060139430869/yoyos-pineapple-bear

  The `barcode` uniquely identifies the product.  The data is stored
  as a raw JSON because that's easier short-term than tring to come up
  with an SQL schema to match the JSON format.
  """

  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:barcode, :string, autogenerate: false}
  @foreign_key_type :string
  schema "food_facts" do
    field(:data, :map)

    timestamps()
  end

  @doc false
  def changeset(food_fact, attrs) do
    food_fact
    |> cast(attrs, [:barcode, :data])
    |> validate_required([:barcode, :data])
  end
end
