defmodule App.InfinityCatalog do
  @moduledoc """
  Interface to Infinity Foods catalog.  There are functions here:

  - to download the catalog and save it to disk,
  - load from disk into a PostgreSQL table,
  - query the PostgreSQL table
  """

  alias App.Catalog
  alias App.Catalog.Product
  alias App.Repo

  require Logger

  @infinity_foods_id "Infinity Foods"

  NimbleCSV.define(MyParser, separator: ",", escape: "\"", encoding: :latin1)

  @doc "Download the Infinity Foods catalog, test parsing, and save to disk."
  @spec download_catalog() :: :ok | {:error, any()}
  def download_catalog do
    url = Application.fetch_env!(:app, :infinity_foods_catalog_url)
    disk_path = Application.fetch_env!(:app, :infinity_foods_catalog_path)

    with {:ok, resp} <- Finch.build("GET", url) |> Finch.request(MyFinch),
         parsed <- parse_csv(resp.body),
         :ok <- validate(parsed) do
      Logger.info("Saving #{disk_path}")
      File.write(disk_path, resp.body)
    else
      {:error, error} ->
        {:error, "InfinityCatalog.download_catalog() failed: #{inspect(error)}"}

      error ->
        {:error, "InfinityCatalog.download_catalog() failed: #{inspect(error)}"}
    end
  end

  @doc "Load the CSV from disk into PostgreSQL."
  @spec load_csv() :: :ok
  def load_csv do
    disk_path = Application.fetch_env!(:app, :infinity_foods_catalog_path)

    with {:ok, text} <- File.read(disk_path),
         parsed <- parse_csv(text),
         :ok <- validate(parsed) do
      replace_products(parsed)
    end
  end

  @doc """
  Load given products into the database.  Products that are in the
  database for Infinity Foods, but not in the list are removed.
  Changed products are updated.  New products are inserted.
  """
  @spec replace_products([map()]) :: :ok
  def replace_products(products) do
    new_map =
      for product <- products, into: %{} do
        {{@infinity_foods_id, product.product_code},
         {:new, Map.put(product, :wholesaler, @infinity_foods_id)}}
      end

    old_map =
      for product <- Catalog.list_products(), into: %{} do
        {{product.wholesaler, product.product_code}, {:old, product}}
      end

    updated_products =
      Map.merge(old_map, new_map, fn _, {:old, old_product}, {:new, new_product} ->
        {:updated, Product.changeset(old_product, new_product)}
      end)

    for {{@infinity_foods_id, _}, product} <- updated_products do
      case product do
        {:updated, changeset} -> Repo.update(changeset)
        {:new, new_product} -> Repo.insert(Product.changeset(%Product{}, new_product))
        {:old, old_product} -> Repo.delete(old_product)
      end
      |> case do
        {:ok, _} -> :ok
        {:error, changeset} -> Logger.error("Error updating product: #{inspect(changeset)}")
      end
    end

    Catalog.refresh_products_search_mv()

    :ok
  end

  @doc "Parse a CSV and see if it works"
  @spec parse_csv(String.t()) :: [map()]
  def parse_csv(csv) do
    MyParser.parse_string(csv)
    |> Enum.flat_map(fn [
                          active_as_a_number,
                          product_code,
                          case_size,
                          organic,
                          description,
                          rrp,
                          brand,
                          _,
                          case_price,
                          _,
                          vat_per_case,
                          barcode,
                          units_per_case,
                          pack_size,
                          unit,
                          vat_rating
                        ] = row ->
      with {:ok, active} <- parse_active_as_a_number(active_as_a_number),
           organic <- parse_organic(organic),
           {:ok, rrp} <- parse_rrp(rrp),
           {case_price, ""} <- Float.parse(case_price),
           {:ok, units_per_case} <- parse_units_per_case(units_per_case),
           barcode <- parse_barcode(barcode) do
        [
          %{
            active: active,
            product_code: product_code,
            organic: organic,
            description: description,
            rrp: rrp,
            brand: brand,
            case_price: case_price,
            units_per_case: units_per_case,
            case_size: case_size,
            pack_size: pack_size,
            unit: unit,
            barcode: barcode,
            vat_per_case: vat_per_case,
            vat_rating: vat_rating
          }
        ]
      else
        error ->
          Logger.info(
            "Failed to parse Infinity Foods catalog row #{inspect(row)}: #{inspect(error)}"
          )

          []
      end
    end)
  end

  defp parse_active_as_a_number("0"), do: {:ok, false}
  defp parse_active_as_a_number("1"), do: {:ok, true}

  defp parse_active_as_a_number(str) do
    {:error, "Can't parse active_as_a_number from #{str}"}
  end

  defp parse_organic("organic"), do: true
  defp parse_organic(_), do: false

  defp parse_units_per_case(""), do: {:ok, 1}

  defp parse_units_per_case(str) do
    case Integer.parse(str) do
      {n, ""} -> {:ok, n}
      _ -> {:error, "Failed to parse units_per_case: #{str}"}
    end
  end

  defp parse_rrp(""), do: {:ok, nil}

  defp parse_rrp(str) do
    case Float.parse(str) do
      {f, ""} -> {:ok, f}
      _ -> {:error, "Failed to parse rrp: #{str}"}
    end
  end

  defp parse_barcode(""), do: nil
  defp parse_barcode(str) when is_binary(str), do: str

  @spec validate([map()]) :: :ok | {:error, String.t()}
  defp validate(products) do
    if length(products) < 1000 do
      {:error, "Infinity Foods catalog has suspiciously few products: #{length(products)} < 1000"}
    else
      :ok
    end
  end
end
