defmodule App.Catalog do
  @moduledoc """
  The Catalog context.
  """

  import Ecto.Query, warn: false

  alias App.Repo
  alias App.Catalog.{FoodFact, Product}
  alias App.{ObjectStorage, OpenFoodFacts}

  require Logger

  @doc "Returns the list of products."
  def list_products do
    Repo.all(Product)
  end

  @doc "Gets a single product."
  def get_product!(id), do: Repo.get!(Product, id)

  @doc """
  Creates a product.

  ## Examples

      iex> create_product(%{field: value})
      {:ok, %Product{}}

      iex> create_product(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_product(attrs \\ %{}) do
    %Product{}
    |> Product.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a product.

  ## Examples

      iex> update_product(product, %{field: new_value})
      {:ok, %Product{}}

      iex> update_product(product, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_product(%Product{} = product, attrs) do
    product
    |> Product.changeset(attrs)
    |> Repo.update()
  end

  @doc "Deletes a product."
  def delete_product(%Product{} = product) do
    Repo.delete(product)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking product changes.

  ## Examples

      iex> change_product(product)
      %Ecto.Changeset{data: %Product{}}

  """
  def change_product(%Product{} = product, attrs \\ %{}) do
    Product.changeset(product, attrs)
  end

  @doc "Refresh the materialized view that backs the products search."
  def refresh_products_search_mv do
    Repo.query("REFRESH MATERIALIZED VIEW CONCURRENTLY products_search_mv")
  end

  defmodule ProductSearch do
    @moduledoc """
    Fuzzy searches over products.

    See also:
    - https://nathanmlong.com/2018/01/fast-fulltext-search-with-ecto-and-postgresql/
    - http://rachbelaid.com/postgres-full-text-search-is-good-enough/
    """

    import Ecto.Query

    defmacro matching_products_and_ranks(search_string) do
      quote do
        fragment(
          """
            SELECT id,
                   ts_rank(
                     products_search_mv.document, plainto_tsquery(?)
                   ) AS rank
            FROM products_search_mv
            WHERE products_search_mv.document @@ plainto_tsquery(?)
               OR products_search_mv.brand ILIKE ?
               OR products_search_mv.description ILIKE ?
          """,
          ^unquote(search_string),
          ^unquote(search_string),
          ^"%#{unquote(search_string)}%",
          ^"%#{unquote(search_string)}%"
        )
      end
    end

    def go(query, search_string) do
      search_string = normalize(search_string)

      from(product in query,
        join: id_and_rank in matching_products_and_ranks(search_string),
        on: id_and_rank.id == product.id,
        order_by: [desc: id_and_rank.rank]
      )
    end

    defp normalize(search_string) do
      search_string
      |> String.downcase()
      |> String.replace(~r/\n/, " ")
      |> String.replace(~r/\t/, " ")
      |> String.replace(~r/\s{2,}/, " ")
      |> String.trim()
    end
  end

  def query_products(query \\ Product, query)

  def query_products(query, "") do
    query
    |> Repo.all()
  end

  def query_products(query, search_string) do
    ProductSearch.go(query, search_string)
    |> Repo.all()
  end

  @doc """
  Returns the list of food_facts.
  """
  def list_food_facts do
    Repo.all(FoodFact)
  end

  @doc """
  Gets a single food_fact.

  Raises `Ecto.NoResultsError` if the Food fact does not exist.
  """
  def get_food_fact!(barcode) do
    from(f in FoodFact, where: f.barcode == ^barcode)
    |> Repo.one!()
  end

  @doc """
  Creates a food_fact.

  ## Examples

      iex> create_food_fact(%{field: value})
      {:ok, %FoodFact{}}

      iex> create_food_fact(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_food_fact(attrs \\ %{}) do
    %FoodFact{}
    |> FoodFact.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a food_fact.

  ## Examples

      iex> update_food_fact(food_fact, %{field: new_value})
      {:ok, %FoodFact{}}

      iex> update_food_fact(food_fact, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_food_fact(%FoodFact{} = food_fact, attrs) do
    food_fact
    |> FoodFact.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a food_fact.
  """
  def delete_food_fact(%FoodFact{} = food_fact) do
    Repo.delete(food_fact)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking food_fact changes.
  """
  def change_food_fact(%FoodFact{} = food_fact, attrs \\ %{}) do
    FoodFact.changeset(food_fact, attrs)
  end

  @doc """
  Tries to fetch food facts for all products that are missing them.
  It does not try to update existing food facts.
  """
  @spec fetch_missing_food_facts(Keyword.t()) :: :ok
  def fetch_missing_food_facts(opts \\ []) do
    limit = Keyword.get(opts, :limit, 10_000)

    products =
      from(p in App.Catalog.Product,
        left_join: o in App.Catalog.FoodFact,
        on: p.barcode == o.barcode,
        where: not is_nil(p.barcode) and is_nil(o.data),
        limit: ^limit
      )
      |> App.Repo.all()

    for p <- products do
      case App.OpenFoodFacts.fetch(p.barcode) do
        {:error, error} ->
          Logger.error("#{error}")

        :not_found ->
          :ok

        {:ok, data} ->
          case create_food_fact(%{barcode: p.barcode, data: data}) do
            {:ok, _} ->
              :ok

            {:error, changeset} ->
              Logger.error("Failed to insert food fact for #{inspect(p)}: #{inspect(changeset)}")
          end
      end

      Process.sleep(300)
    end

    :ok
  end

  @doc """
  Tries to populate the `Product.main_thumbnail` fields of all
  products which are missing it.  This will involve querying the
  database (e.g. for `OpenFoodFacts`), and potentially downloading the
  image and uploading it to GCE Storage with `ObjectStorage`.
  """
  @spec fetch_missing_images(Keyword.t()) :: :ok
  def fetch_missing_images(opts \\ []) do
    limit = Keyword.get(opts, :limit, 10_000)

    products =
      from(p in Product,
        join: o in FoodFact,
        on: p.barcode == o.barcode,
        where: is_nil(p.main_thumbnail),
        select: {p, o},
        limit: ^limit
      )
      |> Repo.all()

    for {product, food_fact} <- products,
        image_url = OpenFoodFacts.image_url(food_fact),
        not is_nil(image_url) do
      case Finch.build("GET", image_url) |> Finch.request(MyFinch) do
        {:ok, resp} ->
          image = resp.body

          {_, content_type} = Enum.find(resp.headers, fn {key, _} -> key === "content-type" end)

          name = :binary.encode_hex(:crypto.hash(:sha, image))

          with :ok <- ObjectStorage.put_object(name, content_type, image),
               {:ok, _} <-
                 update_product(product, %{main_thumbnail: ObjectStorage.public_url(name)}) do
            :ok
          end

        {:error, error} ->
          {:error, error}

        error ->
          {:error, error}
      end
      |> case do
        :ok ->
          :ok

        {:error, error} ->
          Logger.error("Failed to fetch/store image #{image_url}: #{inspect(error)}")
      end

      Process.sleep(300)
    end

    :ok
  end
end
