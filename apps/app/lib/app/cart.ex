defmodule App.Cart do
  @moduledoc """
  The Cart context.

  For now, we use the Cart abstraction to represent carts (items that have yet
  to be checked out), and orders (group of cart lines that have been checked
  out, and ideally, paid for). The carts are represented by cart lines without
  order ids, and orders by cart lines with order ids.

  """

  import Ecto.Query, warn: false

  alias App.Cart.CartLine
  alias App.Repo

  require Logger

  @doc "Returns the list of cart_lines."
  def list_cart_lines do
    from(c in CartLine, preload: [:product, :user], where: is_nil(c.order_id))
    |> Repo.all()
  end

  @doc "Returns the number of cart lines for the given user."
  def count_cart_lines_for_user(%{id: user_id}) do
    from(c in CartLine, where: c.user_id == ^user_id, where: is_nil(c.order_id), select: count())
    |> Repo.one()
  end

  def count_cart_lines_for_user(temp_user_id) when is_binary(temp_user_id) do
    from(c in CartLine,
      where: c.temp_user_id == ^temp_user_id and is_nil(c.order_id),
      select: count()
    )
    |> Repo.one()
  end

  @doc "List cart lines for the given user."
  def list_cart_lines_for_user(%{id: user_id}) do
    from(c in CartLine, where: c.user_id == ^user_id and is_nil(c.order_id), preload: [:product])
    |> Repo.all()
  end

  def list_cart_lines_for_user(temp_user_id) when is_binary(temp_user_id) do
    from(c in CartLine,
      where: c.temp_user_id == ^temp_user_id and is_nil(c.order_id),
      preload: [:product]
    )
    |> Repo.all()
  end

  def list_cart_lines_for_user(nil) do
    []
  end

  @doc "Gets a single cart_line."
  def get_cart_line!(id), do: Repo.get!(CartLine, id)

  @doc "Creates a cart_line."
  def create_cart_line(attrs \\ %{}) do
    %CartLine{}
    |> CartLine.changeset(attrs)
    |> Repo.insert()
  end

  @doc "Updates a cart_line."
  def update_cart_line(%CartLine{} = cart_line, attrs) do
    cart_line
    |> CartLine.changeset(attrs)
    |> Repo.update()
  end

  @doc "Deletes a cart_line."
  def delete_cart_line(%CartLine{} = cart_line) do
    Repo.delete(cart_line)
  end

  @doc "Returns an `%Ecto.Changeset{}` for tracking cart_line changes."
  def change_cart_line(%CartLine{} = cart_line, attrs \\ %{}) do
    CartLine.changeset(cart_line, attrs)
  end

  @doc "Transfer any cart lines from `user1` to `user2`."
  @spec transfer_cart_lines(map() | String.t(), map() | String.t()) ::
          :ok | {:error, String.t()}
  def transfer_cart_lines(user1, user2) do
    conditions = dynamic([c], ^user_condition(user1) and is_nil(c.order_id))
    query = from(c in CartLine, where: ^conditions)

    cart_lines = query |> Repo.all()

    if [] == cart_lines do
      :ok
    else
      Logger.info(
        "Transferring cart lines from #{inspect(user1)} to #{inspect(user2)}: #{inspect(cart_lines)}"
      )

      user_attr_update =
        case user2 do
          %{id: user_id} -> [user_id: user_id, temp_user_id: nil]
          str when is_binary(str) -> [temp_user_id: str, user_id: nil]
        end

      try do
        {_, _} = Repo.update_all(query, set: user_attr_update)
        :ok
      rescue
        error ->
          {:error,
           "Failed to transfer cart lines from #{inspect(user1)} to #{inspect(user2)}: #{inspect(error)}"}
      end
    end
  end

  @doc """
  Sets the quantity of the given product in a user's cart.
  Transparently handles inserting/updating/deleting.
  """
  @spec update_cart_line_for_user_product(map() | String.t(), integer(), integer()) ::
          {:ok, :added | :updated | :deleted} | {:error, any()}
  def update_cart_line_for_user_product(_user, _product_id, qty)
      when is_integer(qty) and qty < 0 do
    {:error, "Quantity must be positive. Got #{qty}"}
  end

  def update_cart_line_for_user_product(user, product_id, qty)
      when is_integer(product_id) and is_integer(qty) and
             (is_map(user) or is_binary(user)) do
    conditions =
      dynamic([c], ^user_condition(user) and c.product_id == ^product_id and is_nil(c.order_id))

    query = from(c in CartLine, where: ^conditions)

    if qty == 0 do
      {_, _} = Repo.delete_all(query)
      {:ok, :deleted}
    else
      query
      |> Repo.one()
      |> case do
        nil ->
          user_attrs = cart_line_user_attr(user)
          cart_attrs = Map.merge(user_attrs, %{product_id: product_id, quantity: qty})
          {create_cart_line(cart_attrs), :added}

        %CartLine{} = cart_line ->
          {update_cart_line(cart_line, %{quantity: qty}), :updated}
      end
      |> case do
        {{:ok, _}, action} -> {:ok, action}
        err -> err
      end
    end
  end

  def total_vat(user_id) do
    zero = Decimal.new(0)

    cart_lines = list_cart_lines_for_user(%{id: user_id})

    Enum.reduce(cart_lines, zero, fn cart_line, total ->
      Decimal.add(total, CartLine.total_vat(cart_line) || zero)
    end)
  end

  def total_price(user_id) do
    zero = Decimal.new(0)

    cart_lines = list_cart_lines_for_user(%{id: user_id})

    Enum.reduce(cart_lines, zero, fn cart_line, total ->
      Decimal.add(total, CartLine.total_price(cart_line) || zero)
    end)
  end

  defp user_condition(%{id: user_id}) do
    dynamic([c], c.user_id == ^user_id)
  end

  defp user_condition(temp_user_id) when is_binary(temp_user_id) do
    dynamic([c], c.temp_user_id == ^temp_user_id)
  end

  defp cart_line_user_attr(%{id: user_id}) do
    %{user_id: user_id}
  end

  defp cart_line_user_attr(temp_user_id) when is_binary(temp_user_id) do
    %{temp_user_id: temp_user_id}
  end

  @doc """
  Checks out, by setting the order id on the current cart lines
  """
  @spec checkout(integer, binary) :: :ok | {:error, Ecto.Changeset.t()}
  def checkout(user_id, order_id) do
    lines =
      from(c in CartLine,
        preload: [:product, :user],
        where: c.user_id == ^user_id and is_nil(c.order_id)
      )
      |> Repo.all()

    result =
      lines
      |> Enum.reduce({:ok, nil}, fn line, acc ->
        case acc do
          {:ok, _cart_line} -> update_cart_line(line, %{order_id: order_id})
          {:error, _changeset} -> acc
        end
      end)

    case result do
      {:ok, _whatever} -> :ok
      {:error, changeset} -> {:error, changeset}
    end
  end
end
