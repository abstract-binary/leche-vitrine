defmodule App.OpenFoodFacts do
  @moduledoc """
  Interface to the
  [openfoodfacts.org](https://world.openfoodfacts.org/data) live JSON
  API.

  Also contains functions to access JSON fields.
  """

  require Logger

  @open_food_facts_url "https://world.openfoodfacts.org/api/v0/product"

  @doc """
  Fetches the JSON for a given barcode.  This makes no attempt to
  actually parse the data.
  """
  @spec fetch(String.t()) :: {:ok, map()} | :not_found | {:error, String.t()}
  def fetch(barcode) when is_binary(barcode) do
    Logger.info("Fetching OpenFoodFacts for #{barcode}")

    Finch.build(:get, "#{@open_food_facts_url}/#{barcode}.json")
    |> Finch.request(MyFinch)
    |> case do
      {:ok, resp} ->
        case Jason.decode(resp.body) do
          {:ok, %{"status" => 1, "product" => product}} -> {:ok, product}
          {:ok, %{"status" => 0}} -> :not_found
          other -> {:error, "Unknown reply from OpenFoodFacts for #{barcode}: #{inspect(other)}"}
        end

      error ->
        {:error, "Failed to fetch OpenFoodFacts for #{barcode}: #{inspect(error)}"}
    end
  end

  @spec image_url(map()) :: nil | String.t()
  def image_url(%{"image_url" => image_url}), do: image_url
  def image_url(%{data: data}), do: image_url(data)
  def image_url(%{}), do: nil

  @doc "Link to the openfoodfacts.org wiki for this product."
  @spec wiki_link(map()) :: nil | String.t()
  def wiki_link(%{"_id" => id}), do: "https://world.openfoodfacts.org/product/#{id}/"
  def wiki_link(%{}), do: nil
end
