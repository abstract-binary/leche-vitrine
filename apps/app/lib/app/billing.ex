defmodule App.Billing do
  @moduledoc """
  The Billing context.
  """

  import Ecto.Query, warn: false

  alias App.Billing.Order
  alias App.{Cart, Delivery, Repo}
  alias App.Cart.CartLine

  @doc "Returns the list of orders."
  def list_order do
    Repo.all(Order)
  end

  @doc "Gets a single order."
  def get_order!(id), do: Repo.get!(Order, id) |> Repo.preload(run: [:address, :user])

  @doc "Create an order."
  def create_order(attrs \\ %{}) do
    %Order{}
    |> Order.changeset(attrs)
    |> Repo.insert()
  end

  @doc "Checkout the current cart-lines with the given website fee."
  @spec checkout(integer, binary, Decimal.t()) ::
          {:ok, Order.t()} | {:error, Ecto.Changeset.t()} | {:error, String.t()}
  def checkout(user_id, run_id, website_fee) do
    total_price = Decimal.add(Cart.total_price(user_id), website_fee)
    total_vat = Cart.total_vat(user_id)

    run = Delivery.get_run(run_id)

    if Decimal.compare(total_price, run.minimum_order_per_participant) ==
         :lt do
      {:error,
       "Minimum order size not met (minimum: #{run.minimum_order_per_participant |> Decimal.to_string()})"}
    else
      Repo.transaction(fn ->
        {:ok, order} =
          create_order(%{
            user_id: user_id,
            run_id: run_id,
            total_vat: total_vat,
            total_price: total_price,
            website_fee: website_fee,
            status: "Pending"
          })

        case Cart.checkout(user_id, order.id) do
          :ok -> nil
          {:error, changeset} -> Repo.rollback(changeset)
        end

        order
      end)
    end
  end

  @spec list_orders_for_user(%{:id => integer, optional(any) => any}) :: [Order.t()]
  def list_orders_for_user(%{id: user_id}) do
    from(a in Order, where: a.user_id == ^user_id, preload: [run: [:user]])
    |> Repo.all()
  end

  @spec list_order_cart_lines(%Order{}) :: [%CartLine{}]
  def list_order_cart_lines(%{id: order_id, user_id: user_id}) do
    from(c in CartLine, where: c.order_id == ^order_id and c.user_id == ^user_id)
    |> Repo.all()
    |> Repo.preload([:product])
  end
end
