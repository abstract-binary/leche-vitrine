defmodule App.User do
  @moduledoc """
  Wrapper around `Legendary.Auth.User`.
  """

  @doc "Returns a humane name for the user."
  @spec friendly_name(map()) :: String.t()
  def friendly_name(%{display_name: nil, email: email}) do
    case String.split(email, "@") do
      [first, _] -> first
      _ -> "anon"
    end
  end

  def friendly_name(%{display_name: display_name}), do: display_name
  def friendly_name(_), do: "anon"

  def revalidate_email(user) do
    user
    |> Ecto.Changeset.cast(
      %{unconfirmed_email: user.email, email_confirmation_token: Ecto.UUID.generate()},
      [:unconfirmed_email, :email_confirmation_token]
    )
    |> App.Repo.update()
  end
end
