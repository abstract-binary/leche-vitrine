defmodule App.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    gce_credentials =
      Application.fetch_env!(:app, :google_application_credentials_json)
      |> case do
        {:dummy_credentials, port} ->
          {:service_account, random_service_account_credentials(),
           url: "http://localhost:#{port}"}

        {:file, file} ->
          {:service_account,
           file
           |> File.read!()
           |> Jason.decode!(), []}

        {:env, var} ->
          {:service_account, System.fetch_env!(var) |> Jason.decode!(), []}
      end

    children = [
      App.Repo,
      # Start the Telemetry supervisor
      AppWeb.Telemetry,
      # Set up the pubsub server
      {Phoenix.PubSub, name: App.PubSub},
      # Start the HTTP client for outgoing requests
      {Finch, name: MyFinch},
      # Start the GCE authentication system
      {Goth, name: MyGoth, source: gce_credentials},
      # Start the Endpoint (http/https)
      AppWeb.Endpoint
      # Start a worker by calling: AppWeb.Worker.start_link(arg)
      # {AppWeb.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: AppWeb.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    AppWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp random_service_account_credentials do
    %{
      "private_key" => random_private_key(),
      "client_email" => "alice@example.com",
      "token_uri" => "/"
    }
  end

  defp random_private_key do
    private_key = :public_key.generate_key({:rsa, 2048, 65_537})
    {:ok, private_key}
    :public_key.pem_encode([:public_key.pem_entry_encode(:RSAPrivateKey, private_key)])
  end
end
