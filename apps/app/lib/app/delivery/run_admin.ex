defmodule App.Delivery.RunAdmin do
  @moduledoc """
  Kaffy config for `Run`.
  """

  alias App.Repo

  def index(_) do
    [
      id: nil,
      user: %{
        value: fn run ->
          run = run |> Repo.preload([:user])
          run.user.email
        end
      },
      order_cutoff: nil,
      estimated_delivery: nil,
      address: %{
        value: fn run ->
          run = run |> Repo.preload([:address])
          run.address.address_line_1
        end
      },
      min_order: %{value: & &1.minimum_order},
      min_order_per_user: %{value: & &1.minimum_order_per_participant},
      wholesaler: nil,
      inserted_at: nil,
      updated_at: nil
    ]
  end
end
