defmodule App.Delivery.Run do
  @moduledoc """
  Basic information tied to a run
  """

  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @derive {Phoenix.Param, key: :id}

  schema "runs" do
    field(:estimated_delivery, :date)
    field(:minimum_order, :decimal)
    field(:minimum_order_per_participant, :decimal)
    field(:order_cutoff, :date)
    field(:wholesaler, :string)
    belongs_to(:user, Legendary.Auth.User)
    belongs_to(:address, App.Delivery.Address)

    timestamps()
  end

  @doc false
  def changeset(run, attrs) do
    run
    |> cast(attrs, [
      :wholesaler,
      :order_cutoff,
      :estimated_delivery,
      :minimum_order,
      :minimum_order_per_participant,
      :user_id,
      :address_id
    ])
    |> validate_required([
      :wholesaler,
      :order_cutoff,
      :estimated_delivery,
      :minimum_order,
      :minimum_order_per_participant,
      :user_id,
      :address_id
    ])
  end

  @spec short_desc(%__MODULE__{}) :: String.t()
  def short_desc(run) do
    "#{run.address.address_line_1} ending #{run.order_cutoff} by #{run.user.email}"
  end
end
