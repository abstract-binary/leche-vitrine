defmodule App.Delivery.Address do
  @moduledoc """
  A delivery address tied to a given user.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "addresses" do
    field(:address_line_1, :string)
    field(:address_line_2, :string)
    field(:city, :string)
    field(:country, :string)
    field(:delivery_instruction, :string)
    field(:full_name, :string)
    field(:phone_number, :string)
    field(:postcode, :string)
    field(:user_id, :id)

    timestamps()
  end

  @doc false
  def changeset(address, attrs) do
    address
    |> cast(attrs, [
      :country,
      :full_name,
      :phone_number,
      :postcode,
      :address_line_1,
      :address_line_2,
      :city,
      :delivery_instruction,
      :user_id
    ])
    |> validate_required([
      :country,
      :full_name,
      :phone_number,
      :postcode,
      :address_line_1,
      :city,
      :user_id
    ])
    |> validate_length(:full_name, min: 2, max: 80)
    |> validate_length(:address_line_1, min: 2, max: 80)
    |> validate_length(:city, min: 2, max: 80)
    |> validate_length(:phone_number, min: 6, max: 20)
    |> validate_length(:delivery_instruction, min: 0, max: 250)
    |> foreign_key_constraint(:user_id)
  end
end
