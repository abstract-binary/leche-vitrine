defmodule App.Delivery.RunParticipant do
  @moduledoc """
  Many-to-many relationship between runs and participants.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "run_participants" do
    belongs_to(:run, App.Delivery.Run, type: :binary_id)
    belongs_to(:user, Legendary.Auth.User)
    timestamps()
  end

  @doc false
  def changeset(participant, attrs) do
    participant
    |> cast(attrs, [:run_id, :user_id])
    |> validate_required([:run_id, :user_id])
    |> unique_constraint([:user_id, :run_id])
  end
end
