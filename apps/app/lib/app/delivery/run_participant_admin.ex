defmodule App.Delivery.RunParticipantAdmin do
  @moduledoc """
  Kaffy config for `RunParticipant`.
  """

  alias App.Delivery.Run
  alias App.Repo

  def index(_) do
    [
      id: nil,
      run: %{
        value: fn rp ->
          rp = rp |> Repo.preload(run: [:address, :user])
          Run.short_desc(rp.run)
        end
      },
      user: %{
        value: fn rp ->
          rp = rp |> Repo.preload([:user])
          rp.user.email
        end
      },
      inserted_at: nil,
      updated_at: nil
    ]
  end
end
