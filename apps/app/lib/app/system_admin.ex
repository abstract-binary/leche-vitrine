defmodule App.SystemAdmin do
  @moduledoc """
  Configuration for Kaffy dashboard.
  """

  import Ecto.Query

  alias App.Repo

  def custom_links(_schema) do
    [
      %{
        name: "Website",
        url: "/",
        order: 1,
        location: :top,
        icon: "home"
      },
      %{
        name: "LiveDashboard",
        url: "/live-dashboard",
        order: 3,
        location: :top,
        icon: "tachometer-alt"
      },
      %{
        name: "Sent Emails (in dev)",
        url: "/sent_emails",
        order: 1,
        location: :top,
        icon: "envelope"
      }
    ]
  end

  def widgets(_schema, _conn) do
    num_products = from(p in App.Catalog.Product, select: count()) |> Repo.one()

    num_products_with_images =
      from(p in App.Catalog.Product, where: not is_nil(p.main_thumbnail), select: count())
      |> Repo.one()

    num_food_facts = from(p in App.Catalog.FoodFact, select: count()) |> Repo.one()

    [
      %{
        type: "tidbit",
        title: "Products",
        content: "#{num_products} (#{num_products_with_images} with images)",
        icon: "bars",
        order: 4,
        width: 2
      },
      %{
        type: "tidbit",
        title: "Food Facts",
        content: "#{num_food_facts}",
        icon: "chart-pie",
        order: 4,
        width: 2
      }
    ]
  end
end
