apiVersion: v1
kind: Namespace
metadata:
  name: leche-vitrine
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: website
  namespace: leche-vitrine
spec:
  replicas: 1
  selector:
    matchLabels:
      app: website
  template:
    metadata:
      labels:
        app: website
    spec:
      containers:
        - name: app
          image: registry.gitlab.com/abstract-binary/leche-vitrine:4.1.0
          command: ["elixir"]
          args:
            - "--name"
            - "$(NAME)@$(POD_IP)"
            - "--cookie"
            - "$(SECRET_KEY_BASE)"
            - "-S"
            - "mix"
            - "phx.server"
          ports:
            - containerPort: 4000
            - containerPort: 4369
          livenessProbe:
            httpGet:
              path: /
              port: 4000
            failureThreshold: 3
            periodSeconds: 10
          startupProbe:
            httpGet:
              path: /
              port: 4000
            failureThreshold: 12
            periodSeconds: 10
          resources:
            requests:
              memory: "512Mi"
              cpu: "250m"
              ephemeral-storage: "200Mi"
            limits:
              memory: "1024Mi"
              cpu: "500m"
              ephemeral-storage: "400Mi"
          env:
            - name: HOSTNAME
              value: leche-vitrine.abstractbinary.org
            - name: EMAIL_FROM
              value: leche-vitrine@mailgun.abstractbinary.org
            - name: NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
            - name: NAME
              value: website
            - name: POOL_SIZE
              value: "4"
            - name: INFINITY_FOODS_CATALOG_PATH
              value: "./priv/infinity_foods.csv"
            - name: INFINITY_FOODS_CATALOG_URL
              value: "https://www.infinityfoodswholesale.coop/download/fde4-f8f2-84a5-9344-dbb9-12e1-ac64-50ce/"
            - name: POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
            - name: DATABASE_URL
              valueFrom:
                secretKeyRef:
                  name: website-config
                  key: database-url
            - name: SECRET_KEY_BASE
              valueFrom:
                secretKeyRef:
                  name: website-config
                  key: secret-key-base
            - name: LIVE_VIEW_SIGNING_SALT
              valueFrom:
                secretKeyRef:
                  name: website-config
                  key: live-view-signing-salt
            - name: MAILGUN_API_KEY
              valueFrom:
                secretKeyRef:
                  name: website-config
                  key: mailgun-api-key
            - name: MAILGUN_DOMAIN
              valueFrom:
                secretKeyRef:
                  name: website-config
                  key: mailgun-domain
            - name: GCE_KEY
              valueFrom:
                secretKeyRef:
                  name: website-config
                  key: gce-key
      imagePullSecrets:
      - name: gitlab-registry
---
apiVersion: v1
kind: Service
metadata:
  name: website
  namespace: leche-vitrine
spec:
  selector:
    app: website
  ports:
    - protocol: TCP
      port: 80
      targetPort: 4000
---
# This is a dummy service to help with libcluster's node detection
# https://www.poeticoding.com/connecting-elixir-nodes-with-libcluster-locally-and-on-kubernetes/
apiVersion: v1
kind: Service
metadata:
  name: website-nodes
  namespace: leche-vitrine
spec:
  clusterIP: None
  selector:
    app: website
  ports:
    - protocol: TCP
      port: 1
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: leche-vitrine
  namespace: leche-vitrine
  annotations:
    kubernetes.io/ingress.global-static-ip-name: leche-vitrine-ab
    kubernetes.io/ingress.global-static-ip-name: leche-vitrine-ab-https
    kubernetes.io/ingress.class: gce
    networking.gke.io/managed-certificates: leche-vitrine-ab
spec:
  defaultBackend:
    service:
      name: website
      port:
        number: 80
---
apiVersion: networking.gke.io/v1
kind: ManagedCertificate
metadata:
  name: leche-vitrine-ab
spec:
  domains:
    - leche-vitrine.abstractbinary.org
---
# This allows libcluster to find the other nodes
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  namespace: leche-vitrine
  name: pod-watcher
rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["list"]
---
# This allows libcluster to find the other nodes
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  namespace: leche-vitrine
  name: pod-watcher-binding
subjects:
- kind: ServiceAccount
  namespace: leche-vitrine
  name: default
roleRef:
  kind: Role
  name: pod-watcher
  apiGroup: rbac.authorization.k8s.io
